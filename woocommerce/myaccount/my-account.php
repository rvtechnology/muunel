<?php
/**
 * My Account page
 *
 * Blueowl custom
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="customAccountPage">
	<div class="customAccountPage__header alignfull">
		<h2><?=__('My Account', 'muunel');?></h2>
		<p class="paragraph-3"><?=__('Welcome Back, ', 'muunel').$current_user->display_name.'!';?></p>
	</div>

	<div class="customAccountPage__wrapper alignfull">
		<div class="customAccountPage__menu">
			<?php /**
			 * My Account navigation.
			 *
			 * @since 2.6.0
			 */
			do_action( 'woocommerce_account_navigation' ); ?>
		</div>
		

		<div class="customAccountPage__content">
			<?php
				/**
				 * My Account content.
				 *
				 * @since 2.6.0
				 */
				do_action( 'woocommerce_account_content' );
			?>
		</div>
	</div>
</div>

