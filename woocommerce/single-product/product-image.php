<?php
/**
 * Single Product Image Full Custom bases on 
 * Product gallery
 * Splide Slider
 * simple-lightbox
 * 
 * Blueowl custom
 *
 */

defined( 'ABSPATH' ) || exit;

global $product;

$post_thumbnail_id = $product->get_image_id();
$attachment_ids = $product->get_gallery_image_ids();
$image_i = 1;
$imageThumbnail_i = 1;
$product_name = $product->get_title();
$default_attributes = $product->get_default_attributes();
?>


	
<div class="productCustomGallery">
	<div id="primary-slider" class="splide splideProductCard">
		<div class="splide__track">
			<ul class="splide__list">
				
			</ul>
		</div>
	</div>
	
	

	
	<div id="secondaryslider">

  <div class="splide__track" id="splide-track">
		<ul class="splide__list custom_splide">
		
 
		</ul> <!-- /.splide__list -->
	</div> <!-- /.splide__track -->
</div> <!-- /.splide -->
	
</div>
