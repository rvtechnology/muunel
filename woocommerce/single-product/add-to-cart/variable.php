<?php
/**
 * Variable product add to cart
 *
 * Blueowl custom
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.5
 */

defined( 'ABSPATH' ) || exit;

global $product;

$attribute_keys  = array_keys( $attributes );
$variations_json = wp_json_encode( $available_variations );
$variations_attr = function_exists( 'wc_esc_json' ) ? wc_esc_json( $variations_json ) : _wp_specialchars( $variations_json, ENT_QUOTES, 'UTF-8', true );

do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<form class="variations_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>" data-product_variations="<?php echo $variations_attr; // WPCS: XSS ok. ?>">
	<?php do_action( 'woocommerce_before_variations_form' ); ?>

	<?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
		<p class="stock out-of-stock"><?php echo esc_html( apply_filters( 'woocommerce_out_of_stock_message', __( 'This product is currently out of stock and unavailable.', 'woocommerce' ) ) ); ?></p>
	<?php else : ?>
	<div class="woo-main-custom">
 <div class="woocommerce-variation-price"><span><?php echo __("Starting from", "muunel");?>: </span><span class="price"></span></div>
<a href="https://www.trustpilot.com/review/muunel.com" target="_blank"><img src="/wp-content/uploads/2021/09/Frame-1268.png" class="star_icon"></a>
  <script type="text/javascript">
jQuery(function(){
    jQuery('.customer-ques').attr('id', 'customer-ques-id');
    jQuery('#button').click(function(){ 
       
                jQuery('#iframeHolder').html('<iframe id="iframe" src="https://widget.trustpilot.com/trustboxes/5418052cfbfb950d88702476/popup.html" width="400" height="450"></iframe>');
        
    });   
});
</script>
<div class="answer-ques">
<div id="toggle">Answred Questions</div>
<script>
jQuery("#toggle").click(function() {
    jQuery('html,body').animate({
        scrollTop: jQuery("#customer-ques-id").offset().top - 100},
        'slow');
});   
jQuery( document ).ready(function() {
var numItems = jQuery('.glsr-review').length;
jQuery("#toggle").before("<p> "
                    + numItems);
});
</script>
</div>

</div>

<div class="woo-custom-measure">
		<table class="variations" cellspacing="0">
			<tbody>
				<?php foreach ( $attributes as $attribute_name => $options ) : ?>
					<tr>
						<td class="value">
							<?php
								wc_dropdown_variation_attribute_options(
									array(
										'options'   => $options,
										'attribute' => $attribute_name,
										'product'   => $product,
										'show_option_none' => wc_attribute_label( $attribute_name ),
									)
								);
								// echo end( $attribute_keys ) === $attribute_name ? wp_kses_post( apply_filters( 'woocommerce_reset_variations_link', '<div class="reset_variations"><a class="button__muunel " href="#">' . esc_html__( 'Clear', 'woocommerce' ) . '</a></div>' ) ) : '';
							?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<div class="woocommerce-variation-customField">
		 <span id="measurementsModalBtn"><?=__("View measurments", "muunel");?></span>
		 </div>
				 
				    
<div class="measurementsModalWysiwyg"><?=get_field('measurments_wysiwyg'); ?>

        </div>
</div>

		<div class="single_variation_wrap">
			<?php
				/**
				 * Hook: woocommerce_before_single_variation.
				 */
				do_action( 'woocommerce_before_single_variation' );

				/**
				 * Hook: woocommerce_single_variation. Used to output the cart button and placeholder for variation data.
				 *
				 * @since 2.4.0
				 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
				 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
				 */
				do_action( 'woocommerce_single_variation' );

				/**
				 * Hook: woocommerce_after_single_variation.
				 */
				do_action( 'woocommerce_after_single_variation' );
			?>
		</div>
	<?php endif; ?>

	<?php do_action( 'woocommerce_after_variations_form' ); ?>
</form>

<?php
do_action( 'woocommerce_after_add_to_cart_form' );
?>
