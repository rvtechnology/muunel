<?php
/**
 * Single variation cart button
 * 
 *  Blueowl custom
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
do_action( 'woocommerce_before_add_to_cart_button' );