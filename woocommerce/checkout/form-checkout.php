<?php
/**
 * Checkout Form
 *
 * Blueowl custom
 * 
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_checkout_form', $checkout );

?>
<div class="alignfull checkoutContainer" data-field-error="<?=__('Incorrect field', 'muunel');?>">
	<div class="checkoutContainer--process">
		<div class="checkout-timeline" id="progressbar">
			<h2 id="checkout-heading"><?= is_user_logged_in() ? __("Billing", "muunel") : __("Login", "muunel"); ?></h2>
			<div class="checkout-timeline-wrapper">
				<div id="cart-item-checkout" class="step done" data-cart="<?=wc_get_cart_url();?>">
					<div class="title-box">
						<div class="circle"></div>
						<p><?php echo __("Cart", "muunel") ?></p>
					</div>
				</div>	
				<div class="line done">
					<span class="line"></span>
				</div>
				<?php if (!is_user_logged_in()) : ?>
				<div class="single-tab-step current" data-title="<?php echo esc_attr(__("Login", "muunel")); ?>">
					<div class="title-box">
						<div class="circle"></div>
						<p><?php echo __("Login", "muunel") ?></p>
					</div>
				</div>
				<div class="line-box">
					<span class="line"></span>
				</div>
				<?php endif; ?>
				<div class="single-tab-step <?= is_user_logged_in() ? 'current' : ''; ?>" data-title="<?php echo esc_attr(__("Billing", "muunel")); ?>">
					<div class="title-box">
						<div class="circle"></div>
						<p><?php echo __("Billing", "muunel") ?></p>
					</div>
				</div>	
				<div class="line-box">
					<span class="line"></span>
				</div>
				<div class="single-tab-step" data-title="<?php echo esc_attr(__("Shipping", "muunel")); ?>">
					<div class="title-box">
						<div class="circle"></div>
						<p><?php echo __("Shipping", "muunel") ?></p>
					</div>
				</div>	
				<div class="line-box current">
					<span class="line"></span>
				</div>
				<div class="single-tab-step" data-title="<?php echo esc_attr(__("Payment", "muunel")); ?>">
					<div class="title-box">
						<div class="circle"></div>
						<p><?php echo __("Payment", "muunel") ?></p>
					</div>
				</div>		
			</div>
		</div>
		<div class="checkout-form">
			<div class="timeline-content">
				<form name="checkout" method="post" class="checkout woocommerce-checkout customCheckoutPage" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data" id="msform">
					<?php if ( $checkout->get_checkout_fields() ) : ?>

						<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

						<div class="customCheckoutPage__column customCheckoutPage__column--forms" id="customer_details">
							<?php if (!is_user_logged_in()) : ?>
								<fieldset id="loginToCheckout">
									<div class="customCheckoutPage__billing">
										<?php //do_action( 'woocommerce_checkout_billing' ); ?>
									</div>
									<input type="button" name="next" class="next button__muunel" value="<?php echo __("Continue", "muunel"); ?>" />
								</fieldset>
							<?php endif; ?>
							<fieldset id="billing_formset">
								<div class="customCheckoutPage__billing">
									<?php do_action( 'woocommerce_checkout_billing' ); ?>
								</div>
								<input type="button" name="previous" class="previous" value="<?php echo __("Back", "muunel"); ?>" />
								<input type="button" name="next" class="next button__muunel" value="<?php echo __("Continue", "muunel"); ?>" />
							</fieldset>
							<fieldset id="shipping_formset">
								<div class="customCheckoutPage__shipping">
									<?php do_action( 'woocommerce_checkout_shipping' ); ?>
								</div>
								<input type="button" name="previous" class="previous" value="<?php echo __("Back", "muunel"); ?>" />
								<input type="button" name="next" class="next button__muunel" value="<?php echo __("Continue", "muunel"); ?>" />
							</fieldset>
							<fieldset>
								<div class="customCheckoutPage__shipping">
									<?php do_action( 'woocommerce_checkout_order_review' ); ?>
								</div>
								<input type="button" name="previous" class="previous" value="<?php echo __("Back", "muunel"); ?>" />
							</fieldset>
						</div>

						<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

					<?php endif; ?>

				</form>
			</div>
		</div>
	</div>
	
	<div class="customCartPage__sidebar">
		<?=get_sidebar('checkout');?>
	</div>
</div>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>