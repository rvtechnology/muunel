<?php
/**
 * Proceed to checkout button
 *
 * Blueowl custom
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<?php if(is_user_logged_in()) : ?>
<a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="button__muunel button__muunel--fullWidth">
	<?php esc_html_e( 'Proceed to checkout', 'woocommerce' ); ?>
</a>
<?php else : ?>
<a href="<?= get_permalink(wc_get_page_id('myaccount'));?>?action=toCheckout" class="button__muunel button__muunel--fullWidth">
	<?php esc_html_e( 'Proceed to checkout', 'woocommerce' ); ?>
</a>
<?php endif;