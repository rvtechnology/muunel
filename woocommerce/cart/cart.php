<?php
/**
 * Cart Page
 *
 * Blueowl custom
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;
?>

<?php do_action( 'woocommerce_before_cart' ); ?>

<div class="customCartPage alignfull">
	
	<div class="customCartPage__wrapper">

		<div class="customCartPage__form">

			<div class="checkout-timeline no-print" id="progressbar">
				<a href="<?php echo wc_get_page_permalink( 'shop' );?>" class="previous"><?php echo __("Keep shipping", "muunel"); ?></a>
				<h2 id="checkout-heading"><?php echo __("Cart", "muunel") ?></h2>
				<div class="checkout-timeline-wrapper">
					<div class="step current">
						<div class="title-box">
							<div class="circle"></div>
							<p><?php echo __("Cart", "muunel") ?></p>
						</div>
					</div>	
					<div class="line current">
						<span class="line"></span>
					</div>
					<?php if(!is_user_logged_in()): ?>
					<div class="single-tab-step">
						<div class="title-box">
							<div class="circle"></div>
							<p><?php echo __("Login", "muunel") ?></p>
						</div>
					</div>
					<div class="line-box">
						<span class="line"></span>
					</div>
					<?php endif; ?>
					<div class="single-tab-step ">
						<div class="title-box">
							<div class="circle"></div>
							<p><?php echo __("Billing", "muunel") ?></p>
						</div>
					</div>	
					<div class="line-box">
						<span class="line"></span>
					</div>
					<div class="single-tab-step">
						<div class="title-box">
							<div class="circle"></div>
							<p><?php echo __("Shipping", "muunel") ?></p>
						</div>
					</div>	
					<div class="line-box">
						<span class="line"></span>
					</div>
					<div class="single-tab-step">
						<div class="title-box">
							<div class="circle"></div>
							<p><?php echo __("Payment", "muunel") ?></p>
						</div>
					</div>		
				</div>
			</div>
			<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
				<?php do_action( 'woocommerce_before_cart_table' ); ?>
				<?php
				foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$_variation = new WC_Product_Variation($cart_item["variation_id"]);
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					$parent = wc_get_product($_product->get_parent_id());
					$out_of_stock = false;

					// echo "<pre>";
					// 	var_dump($product_permalink);
					// echo "</pre>";
					if(! $_product->is_in_stock()){
						$out_of_stock = true;
					}
					?>
						<div class="single-product-box <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?> woocommerce-cart-form__contents" <?=$out_of_stock ? 'style="border-color:red"' : '';?>>
							<div class="product-thumbnail">
							<?php
								$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_variation->get_image(), $cart_item, $cart_item_key );

								if ( ! $product_permalink ) {
									echo $thumbnail; // PHPCS: XSS ok.
								} else {
									printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
								}
							?>
							</div>
							<div class="product-info" <?= $cart_item['custom_data']['prescription_upload'] == true ? 'style="padding:20px;"' : '';?>>
								<div class="product-title" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
									<?php
										$parent = wc_get_product($_product->get_parent_id());
										// echo get_the_title($_product->get_parent_id());
										if ( ! $product_permalink ) {
											// echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $parent->get_title(), $cart_item, $cart_item_key ) . '&nbsp;' );
										} else {
											// echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s"><h4>%s</h4></a>', esc_url( $product_permalink ), $parent->get_title() ), $cart_item, $cart_item_key ) );
											echo sprintf( '<a href="%s"><h4>%s</h4></a>', esc_url( $product_permalink ), get_the_title($_product->get_parent_id()) );
										}
									?>
									<?php
										
										if ( ! $product_permalink ) {
											echo $_product->get_attribute('pa_color');
											echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_attribute('pa_color'), $cart_item, $cart_item_key ) . '&nbsp;' );
										} else {
											echo sprintf( '<a href="%s"><p>%s</p></a>', esc_url( $product_permalink ), $_product->get_attribute('pa_color') );
										}
									?>
								</div>
								<h5 class="product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
									<?= wc_price($_variation->get_price()); ?>
								</h5>
								<div class="lens-info">
									<p><?=__('Lens:', 'muunel'); ?></p>
									<?php $prescription_name = get_prescription_name_by_slug($cart_item['variation']['attribute_pa_prescriptions']); ?>
									<p class="bolder <?=$cart_item['variation']['attribute_pa_prescriptions'] == "prescription" && $cart_item['custom_data']['prescription_upload'] != true || $cart_item['custom_data']['additional_name'] != "" ? "clickable" : "";?>"><?=$prescription_name?></p>
									<?php if($cart_item['custom_data']['prescription_upload'] == true): ?>
										<p><?=__('Files:', 'muunel');?>
										<?php foreach($cart_item['custom_data']['uploaded_files'] as $file): ?>
											<a style="color:#000;font-weight:600" target="_blank" href="<?=$file['url']?>"><?=$file['title']?></a><br>
										<?php endforeach; ?></p>
									<?php endif; ?>
								</div>
								<p class="lens-price">
									<?php 
										echo wc_price($cart_item['custom_data']['additional_price']);
									?>
								</p>
							</div>
							<div class="product-control">
								<div class="cost-info">
									<h4 class="total-cost" data-title="<?php esc_attr_e( 'Suma', 'woocommerce' ); ?>">
									<?php
										echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
									?>
									</h4>
									<p><?php echo __("Total Cost", "muunel");?></p>
								</div>
								<div class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
								<?php
									if ( $_product->is_sold_individually() ) {
										$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
									} else {
										$product_quantity = woocommerce_quantity_input(
											array(
												'input_name'   => "cart[{$cart_item_key}][qty]",
												'input_value'  => $cart_item['quantity'],
												'max_value'    => $_product->get_max_purchase_quantity(),
												'min_value'    => '0',
												'product_name' => $_product->get_name(),
											),
											$_product,
											false
										);
									}

									echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
									?>
											
								</div>
								<div class="product-remove no-print">
								<?php
									echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
										'woocommerce_cart_item_remove_link',
										sprintf(
											'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><div class="close"></div><div>' . __("Delete", "muunel") . '</div></a>',
											esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
											esc_html__( 'Remove this item', 'woocommerce' ),
											esc_attr( $product_id ),
											esc_attr( $_product->get_sku() )
										),
										$cart_item_key
									);
								?>
								</div>
							</div>
							<?php if($out_of_stock): ?>
								<p style="color:red"><?=__('We are sorry but this product is out of stock', 'muunel');?></p>
							<?php endif; ?>
							<?php if($cart_item['variation']['attribute_pa_prescriptions'] == "non-prescription" && $cart_item['custom_data']['additional_name'] != "") : ?>
							<div class="lens-table additional-name">
								<p><?php echo $cart_item['custom_data']['additional_name']; ?></p>
							</div>
							<?php endif; ?>
							<?php if($cart_item['variation']['attribute_pa_prescriptions'] == 'prescription' && $cart_item['custom_data']['prescription_upload'] != true):?>
							<div class="lens-table">
								<table>
									<tr>
										<td></td>
										<td><p class="paragraph-2"><?=__('Sphere (SPH)', 'muunel');?></p></td>
										<td><p class="paragraph-2"><?=__('Cylinder (CYL)', 'muunel');?></p></td>
										<td><p class="paragraph-2"><?=__('Axis', 'muunel');?></p></td>
									</tr>
									<tr>
										<td><p class="paragraph-2"><?=__('OD', 'muunel');?></p><p class="paragraph-5"><?=__('Right Eye', 'muunel');?></p></td>
										<td>
											<p class="muunel__dropdown__trigger right_eye_sphere"><?php echo $cart_item['custom_data']['prescription_data']['right_eye_sphere'];?></p>
										</td>
										<td>
											<p class="muunel__dropdown__trigger right_eye_cylinder"><?php echo $cart_item['custom_data']['prescription_data']['right_eye_cylinder'];?></p>
										</td>
										<td>
											<p class="muunel__dropdown__trigger right_axis"><?php echo $cart_item['custom_data']['prescription_data']['right_axis'];?></p>
										</td>
									</tr>
									<tr>
										<td><p class="paragraph-2"><?=__('OS', 'muunel');?></p><p class="paragraph-5"><?=__('Left Eye', 'muunel');?></p></td>
										<td>
											<p class="muunel__dropdown__trigger left_eye_sphere">
												<?php echo $cart_item['custom_data']['prescription_data']['left_eye_sphere'];?>
											</p>
										</td>
										<td>
											<p class="muunel__dropdown__trigger left_eye_cylinder"><?php echo $cart_item['custom_data']['prescription_data']['left_eye_cylinder'];?></p>
										</td>
										<td>
											<p class="muunel__dropdown__trigger left_axis"><?php echo $cart_item['custom_data']['prescription_data']['left_axis'];?></p>
										</td>
									</tr>
									<?php if(!empty($cart_item['custom_data']['prescription_data']['single_pd']) ||
									!empty($cart_item['custom_data']['prescription_data']['left_pd']) ||
									!empty($cart_item['custom_data']['prescription_data']['right_pd'])):?>
									<tr>
										<td><p class="paragraph-2"><?=__('PD', 'muunel');?></p><p class="paragraph-5"><?=__('Popillary Distance', 'muunel');?></p></td>
										<?php if(!empty($cart_item['custom_data']['prescription_data']['single_pd'])):?>
										<td class="single_pd">
											<p class="muunel__dropdown__trigger single_pd"><?php echo $cart_item['custom_data']['prescription_data']['single_pd'];?></p>
										</td>
										<?php else: ?>
										<td class="left_pd">
											<p class="muunel__dropdown__trigger left_pd"><?php echo $cart_item['custom_data']['prescription_data']['left_pd'];?></p>
										</td>
										<td class="right_pd">
											<p class="muunel__dropdown__trigger right_pd"><?php echo $cart_item['custom_data']['prescription_data']['right_pd'];?></p>
										</td>
										<?php endif;?>
									</tr>
									<?php endif; ?>
								</table>
							</div>
							<?php endif;?>
						</div>
					<?php
					}
				}
				?>
				<div class="actions" style="display: none">
					<?php if ( wc_coupons_enabled() ) { ?>
						<div class="coupon">
							<label for="coupon_code"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?=__('Posiadasz kupon rabatowy?', 'muunel');?>" /> <button type="submit" class="button__muunel button&__kanda" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?=__('Akceptuj', 'muunel');?></button>
							<?php do_action( 'woocommerce_cart_coupon' ); ?>
						</div>
					<?php } ?>
					<button type="submit" class="button__muunel" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?=__('Zaktualizuj', 'muunel');?></button>
					<?php do_action( 'woocommerce_cart_actions' ); ?>
					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
					<?php do_action( 'woocommerce_after_cart_contents' ); ?>
				</div>
				<?php do_action( 'woocommerce_after_cart_table' ); ?>
			</form>
		</div>
		<?php if(get_field('content')): ?>
		<div class="additional-cart-box no-print" style="background-color:<?php echo get_field('backgorund_color');?>">
			<div class="content">
				<?php echo get_field('content'); ?>
			</div>
			<div class="cute-1"></div>
			<div class="cute-2"></div>
		</div>
		<?php endif;?>
		<div class="customCartPage__sidebar">
			<?=get_sidebar('checkout');?>
		</div>

		<div class="customCartPage__collaterals">
			<?php do_action( 'woocommerce_before_cart_collaterals' ); ?>
			<div class="cart-collaterals">
				<?php
					/**
					 * Cart collaterals hook.
					 *
					 * @hooked woocommerce_cross_sell_display
					 * @hooked woocommerce_cart_totals - 10
					 */
					// remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
					// do_action( 'woocommerce_cart_collaterals' );
				?>
			</div>
		</div>
	</div>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>
