<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Blueowl custom
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

$product_id = $product->get_id();
$terms = get_the_terms ( $product_id, 'product_cat' );
$excerpt = get_field('product_excerpt');
// $file = get_field('file', 'product_cat' . '_' . $terms[0]->term_id);
// $repeater = get_field('faq_repeater', 'product_cat' . '_' . $terms[0]->term_id);

?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'customProductPage', $product ); ?>>
	<?php
		if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb( '<p id="breadcrumbs" class="alignfull">','</p>' );
		}
	?>
	<section class="section customProductPage__headerWrapper alignfull">
        <div class="product_titleBox">
            <?php woocommerce_template_single_title();?>
        </div>
		<div class="customProductPage__gallery">
			<?php
			/**
			 * Hook: woocommerce_before_single_product_summary.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );
			?>
		</div>

		<div class="customProductPage__summary">
			<div class="customProductPage__summaryWrapper">
			    <div class="location-main">
				<div class="location-left">Deliver to Romania</div>
				<div class="delivery-week">Arrives: <?php $date = strtotime("+7 day");
echo date('l, M d', $date);
?></div>	
				</div>
				<button class="customProductPage__summaryWrapper--shareButton" id="shareModalBtn">
					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M18 19L6 12L18 5" stroke="black" stroke-width="1.5"/>
						<circle cx="18" cy="5" r="3" fill="white" stroke="black" stroke-width="1.5"/>
						<circle cx="6" cy="12" r="3" fill="white" stroke="black" stroke-width="1.5"/>
						<circle cx="18" cy="19" r="3" fill="white" stroke="black" stroke-width="1.5"/>
					</svg>
				</button>
                <div class="product_titleBox">
                    <?php
                    /**
                     * Hook: woocommerce_single_product_summary.
                     *
                     * @hooked woocommerce_template_single_title - 5
                     * @hooked woocommerce_template_single_rating - 10
                     * @hooked woocommerce_template_single_price - 10
                     * @hooked woocommerce_template_single_excerpt - 20
                     * @hooked woocommerce_template_single_add_to_cart - 30
                     * @hooked woocommerce_template_single_meta - 40
                     * @hooked woocommerce_template_single_sharing - 50
                     * @hooked WC_Structured_Data::generate_product_data() - 60
                     */
                    // add_action('woocommerce_single_product_summary', 'the_content', 20 );

                    woocommerce_template_single_title();
                    ?>
                </div>

				<div class="customProductPage__summaryWrapper--excerpt"><?php woocommerce_template_single_excerpt(); ?></div>
				<?php
				woocommerce_template_single_add_to_cart();
				?>
				<div id="lenses_modal_trigger" error-message="<?=esc_attr(__('Product is out of stock!', 'muunel'));?>" class="button__muunel">
				<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M2 10H22L19 21H5L2 10Z" stroke="black" stroke-width="1.5" stroke-linecap="round"></path>
            <path d="M12 18V13" stroke="black" stroke-width="1.5"></path>
            <path d="M16 18L17.5 13" stroke="black" stroke-width="1.5"></path>
            <path d="M8 18L6.5 13" stroke="black" stroke-width="1.5"></path>
            <path d="M5 10L9 4.5" stroke="black" stroke-width="1.5"></path>
            <path d="M19 10L15 4.5" stroke="black" stroke-width="1.5"></path>
          </svg><?=__('Select Lenses & Purchase', 'muunel');?>
				</div>
				<div class="secure"><button id="myBtn" class="secure_payment">Secure Payment</button>
								<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>We work hard to protect your security and privacy. Our payment security system encrypts your information during transmission. We don’t share your credit card details with third-party sellers, and we don’t sell your information to others.</p>
  </div>

</div>


				</div>
				
				


				<div class="ron">
					<p  class="spend"><strong>Kit & Save</strong><img id="myBtn1" class="kit_save" src="/wp-content/uploads/2021/09/icons8-ask-question-50-1.png"></p>
					<p>Spend <strong>344 Ron</strong>, Get <strong>10% Off</strong> + Free Shipping</p>
					
													<!-- The Modal -->
<div id="myModal1" class="modal1">

  <!-- Modal content -->
  <div class="modal-content1">
    <span class="close1">&times;</span>
    <p class="list_ron">
        <ul><li>Spend 350 Ron = 10% Off</li>
        <li>Spend 550 Ron = 20% Off + Free shipping</li></ul>
        </p>
  </div>

</div>
					</div>
					<?php// echo do_shortcode('[wptabs id="7752"]'); ?>
					




				</div>
			</div>
		</div>
	</section>
</div>


	<style>


/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
/* The Modal (background) */
.modal1 {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content1 {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
}

/* The Close Button */
.close1 {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close1:hover,
.close1:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
.modal2 {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content2 {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
}

/* The Close Button */
.close2 {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close2:hover,
.close2:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>
<!-- Trigger/Open The Modal -->



<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
   modal1.style.display = "none";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

<script>
// Get the modal
var modal1 = document.getElementById("myModal1");

// Get the button that opens the modal
var btn1 = document.getElementById("myBtn1");

// Get the <span> element that closes the modal
var span1 = document.getElementsByClassName("close1")[0];

// When the user clicks the button, open the modal 
btn1.onclick = function() {
  modal1.style.display = "block";
  modal.style.display = "none";
}

// When the user clicks on <span> (x), close the modal
span1.onclick = function() {
  modal1.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.addEventListener("click", function(event) {
   
    if (event.target == modal1) {
       modal1.style.display = "none";
    }
});


// Get the modal
var modal2 = document.getElementById("myModal2");

// Get the button that opens the modal
var btn2 = document.getElementById("myBtn2");

// Get the <span> element that closes the modal
var span2 = document.getElementsByClassName("close2")[0];

// When the user clicks the button, open the modal 
btn2.onclick = function() {
  modal1.style.display = "none";
  modal2.style.display = "block";
  modal.style.display = "none";
}

// When the user clicks on <span> (x), close the modal
span2.onclick = function() {
  modal2.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.addEventListener("click", function(event) {
   
    if (event.target == modal1) {
       modal2.style.display = "none";
    }
});

jQuery( document ).ready(function() {
var numItems = jQuery('.glsr-review').length;
jQuery("#toggle").before("<p> "
                    + numItems);
});
</script>



	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	// do_action( 'woocommerce_after_single_product_summary' );
	?>

</div>

<?php do_action( 'woocommerce_after_single_product' );?>
<?php the_content(); ?>