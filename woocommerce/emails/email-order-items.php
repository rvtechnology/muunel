<?php
/**
 * Email Order Items
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-items.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$text_align  = is_rtl() ? 'right' : 'left';
$margin_side = is_rtl() ? 'left' : 'right';

foreach ( $items as $item_id => $item ) :
	$product       = $item->get_product();
	$sku           = '';
	$purchase_note = '';
	$image         = '';

	if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
		continue;
	}

	if ( is_object( $product ) ) {
		$sku           = $product->get_sku();
		$purchase_note = $product->get_purchase_note();
		$image         = $product->get_image( $image_size );
	}

	?>
	<tr style="border: 1px solid #D8D8D8" class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
		<td colspan="2" class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; border-bottom: 1px solid #D8D8D8; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; border-top: 1px solid #D8D8D8; border-left: 1px solid #D8D8D8;">
		<?php

		// Show title/image etc.
		if ( $show_image ) {
			echo wp_kses_post( apply_filters( 'woocommerce_order_item_thumbnail', $image, $item ) );
		}
		
		// Product name.
        ?>
        <div style="display: inline-block; width: auto">
            <?php
                echo wp_kses_post( apply_filters( 'woocommerce_order_item_name', $product->get_title(), $item, false ) );
                echo "<br>";
                echo $product->get_attribute('pa_color');
            ?>
        </div>
        <?php
		
		// SKU.
		// if ( $show_sku && $sku ) {
		// 	echo wp_kses_post( ' (#' . $sku . ')' );
		// }

		?>
		</td>
		<td class="td" style="border-top: 1px solid #D8D8D8; border-bottom: 1px solid #D8D8D8; text-align: center;">
			<?php
			$qty          = $item->get_quantity();
			$refunded_qty = $order->get_qty_refunded_for_item( $item_id );

			if ( $refunded_qty ) {
				$qty_display = '<del>' . esc_html( $qty ) . '</del> <ins>' . esc_html( $qty - ( $refunded_qty * -1 ) ) . '</ins>';
			} else {
				$qty_display = esc_html( $qty );
			}
			echo wp_kses_post( apply_filters( 'woocommerce_email_order_item_quantity', $qty_display, $item ) );
			?>
		</td>
		<td class="td" style="text-align: right; font-weight: 600; border-bottom: 1px solid #D8D8D8; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; border-top: 1px solid #D8D8D8; border-right: 1px solid #D8D8D8;"><?php echo wp_kses_post( $order->get_formatted_line_subtotal( $item ) ); ?></td>
	</tr>
	<tr>
		<td class="td" style="border-left: 1px solid #D8D8D8;" colspan="2">
			<?= __('Lens', 'muunel');?>: <?php echo $product->get_attribute('pa_prescriptions');?>
		</td>
	</tr>
	<?php if($item->get_meta('Right Eye Cylinder(CYL)') != "" || $item->get_meta('Left Eye Cylinder(CYL)') != "" || $item->get_meta('Right Eye Sphere(SPH)') != "" || $item->get_meta('Left Eye Sphere(SPH)') != "") :  ?>
		<tr class="lens-table-row" >
			<td class="td lens-table-td" ></td>
			<td class="td lens-table-td" ><p class="paragraph-2" style="margin-bottom: 0px;"><?=__('Sphere (SPH)', 'muunel');?></p></td>
			<td class="td lens-table-td" ><p class="paragraph-2" style="margin-bottom: 0px;"><?=__('Cylinder (CYL)', 'muunel');?></p></td>
			<td class="td lens-table-td" ><p class="paragraph-2" style="margin-bottom: 0px;"><?=__('Axis', 'muunel');?></p></td>
		</tr>
		<tr class="lens-table-row">
			<td class="td lens-table-td" ><p class="paragraph-2" style="margin-bottom: 0px;"><?=__('OD', 'muunel');?></p><p class="paragraph-5" style="margin-bottom: 0px;"><?=__('Right Eye', 'muunel');?></p></td>
			<td class="td lens-table-td" >
				<p class="muunel__dropdown__trigger right_eye_sphere" style="margin-bottom: 0px;"><?php echo $item->get_meta('Right Eye Sphere(SPH)');?></p>
			</td>
			<td class="td lens-table-td" >
				<p class="muunel__dropdown__trigger right_eye_cylinder" style="margin-bottom: 0px;"><?php echo $item->get_meta('Right Eye Cylinder(CYL)');?></p>
			</td>
			<td class="td lens-table-td" >
				<p class="muunel__dropdown__trigger right_axis" style="margin-bottom: 0px;"><?php echo $item->get_meta('Right Eye Axis');?></p>
			</td>
		</tr>
		<tr class="lens-table-row">
			<td class="td lens-table-td" ><p class="paragraph-2" style="margin-bottom: 0px;"><?=__('OS', 'muunel');?></p><p class="paragraph-5" style="margin-bottom: 0px;"><?=__('Left Eye', 'muunel');?></p></td>
			<td class="td lens-table-td" >
				<p class="muunel__dropdown__trigger left_eye_sphere" style="margin-bottom: 0px;">
					<?php echo $item->get_meta('Left Eye Sphere(SPH)');?>
				</p>
			</td>
			<td class="td lens-table-td" >
				<p class="muunel__dropdown__trigger left_eye_cylinder" style="margin-bottom: 0px;"><?php echo $item->get_meta('Left Eye Cylinder(CYL)');?></p>
			</td>
			<td class="td lens-table-td" >
				<p class="muunel__dropdown__trigger left_axis" style="margin-bottom: 0px;"><?php echo $item->get_meta('Left Eye Axis');?></p>
			</td>
		</tr>
		<?php if(!empty($item->get_meta('Single Eye Pupillary Distance(PD)')) || !empty($item->get_meta('Left Eye Pupillary Distance(PD)')) || !empty($item->get_meta('Right Eye Pupillary Distance(PD)'))): ?>
			<tr class="lens-table-row">
				<td class="td lens-table-td" ><p class="paragraph-2" style="margin-bottom: 0px;"><?=__('PD', 'muunel');?></p><p class="paragraph-5" style="margin-bottom: 0px;"><?=__('Popillary Distance', 'muunel');?></p></td>
				<?php if(!empty($item->get_meta('Single Eye Pupillary Distance(PD)'))):?>
					<td class="td lens-table-td single_pd">
						<p class="muunel__dropdown__trigger single_pd" style="margin-bottom: 0px;"><?php echo $item->get_meta('Single Eye Pupillary Distance(PD)');?></p>
					</td>
					<?php else: ?>
					<td class="td lens-table-td left_pd">
						<p class="muunel__dropdown__trigger left_pd" style="margin-bottom: 0px;"><?php echo $item->get_meta('Left Eye Pupillary Distance(PD)');?></p>
					</td>
					<td class="td lens-table-td right_pd">
						<p class="muunel__dropdown__trigger right_pd" style="margin-bottom: 0px;"><?php echo $item->get_meta('Right Eye Pupillary Distance(PD)');?></p>
					</td>
				<?php endif;?>
			</tr>
		
		<?php endif; ?>
	<?php endif;			
	if ( $show_purchase_note && $purchase_note ) {
		?>
		<tr>
			<td colspan="3" style="text-align:<?php echo esc_attr( $text_align ); ?>; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
				<?php
				echo wp_kses_post( wpautop( do_shortcode( $purchase_note ) ) );
				?>
			</td>
		</tr>
		<?php
	}
	?>

<?php endforeach; ?>
