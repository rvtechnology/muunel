<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
															</div>
														</td>
													</tr>
												</table>
												<!-- End Content -->
											</td>
										</tr>
									</table>
									<!-- End Body -->
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top">
						<!-- Footer -->
						<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer" style="background-color: #000;">
							<tr>
								<td valign="top">
									<table border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td style="text-align:center; padding-top: 20px"> 
												<a href="https://www.instagram.com/muunel.eyewear/?hl=en"><img src="https://muunel.stronazen.pl/wp-content/uploads/2021/07/Group-948.png" alt="insta"></a>
												<a href="https://www.facebook.com/Muunel-100931331637192/"><img src="https://muunel.stronazen.pl/wp-content/uploads/2021/07/Group-32.png" alt="fb"></a>
												<a href="https://www.youtube.com/channel/UCC0Z1DK2Wngno0ShUBo5mZA/videos"><img src="https://muunel.stronazen.pl/wp-content/uploads/2021/07/Twitter.png" alt="youtube"></a>
											</td>
										</tr>
										<tr>
											<td colspan="2" valign="middle" id="credit" style="text-align:center; padding-top: 20px">
												<?php echo __('This email was sent by', 'muunel'); ?> &nbsp; <a style="color: #fff; font-weight: 600" href="https://muunel.stronazen.pl">muunel.ro</a> <br/>Oesterbrogade 226 st 1 Suite #8 Copenhagen, 2100 Denmark
											</td>
										</tr>
										<tr>
											<td colspan="2" valign="middle" id="credit">
												<?php echo wp_kses_post( wpautop( wptexturize( apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) ) ) ) ); ?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End Footer -->
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
