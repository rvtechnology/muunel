<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

if( $product->is_type('variable') ) {
  $terms = wp_get_post_terms( $product->get_id(), 'pa_color', array( 'orderby' => 'menu_order') );
}
?>
    <div id="productCard-<?=$product->get_id()?>" class="customArchiveProduct__loopWrapper--item">
        <?php $active_img = true; ?>
        <?php foreach ($terms as $term) : ?>
            <a  class="variable_image <?php if($active_img) {echo 'active'; $active_img=false;} ?>" data-variable="<?= $term->slug;?>" href="<?=$product->get_permalink()?>?attribute_pa_color=<?= $term->slug;?>">
                <div class="product_images">
                    <?php $variant = wc_get_product(custom_get_variant_id($product->get_id(), $term->slug)); ?>
                    <?= $variant->get_image();?>
                    <?php if(get_field('hover_image', $variant->get_id())) :
                        $hover = get_field('hover_image', $variant->get_id()); ?>
                        <img class="card_image_hover" src="<?=wp_get_attachment_image_src($hover, 'productImage')[0]?>" alt="product-image-hover-<?=$term->slug?>">
                    <?php endif; ?>
                </div>
                <h3><?=$product->get_title();?></h3>
            </a>
        <?php endforeach; ?>

        <div class="variable_buttons">
            <?php $active_button = true; ?>
             <?php foreach ($terms as $term) : ?>
                 <span class="variable_button <?php if($active_button) {echo 'active'; $active_button=false;} ?>" data-variable="<?= $term->slug;?>" href="<?=$product->get_permalink()?>?attribute_pa_color=<?= $term->slug;?>">
                    <span style="background-color: <?= get_term_meta($term->term_id, 'product_attribute_color', true)?>;"></span>
                </span>
             <?php endforeach; ?>
        </div>
        <div class="price"><span class="price__content"><?php echo __("Starting from", "muunel");?>:&nbsp;<?=wc_price($product-> get_variation_regular_price('min'))?></span>

          <?php $active_atc = true; ?>
          <?php foreach ($terms as $term) : ?>
            <span class="variable_add_to_cart <?php if($active_atc) {echo 'active'; $active_atc=false;} ?>" data-variable="<?= $term->slug;?>">
                <p>
                    <a href="<?= wc_get_cart_url();?>/?add-to-cart=<?=$product->get_id();?>&variation_id=<?=custom_get_variant_id($product->get_id(), $term->slug);?>&attribute_pa_color=<?= $term->slug;?>&attribute_pa_prescriptions=non-prescription">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M2 10H22L19 21H5L2 10Z" stroke="black" stroke-width="1.5" stroke-linecap="round"/>
                            <path d="M12 18V13" stroke="black" stroke-width="1.5"/>
                            <path d="M16 18L17.5 13" stroke="black" stroke-width="1.5"/>
                            <path d="M8 18L6.5 13" stroke="black" stroke-width="1.5"/>
                            <path d="M5 10L9 4.5" stroke="black" stroke-width="1.5"/>
                            <path d="M19 10L15 4.5" stroke="black" stroke-width="1.5"/>
                        </svg><?= __('Add To Cart', 'muunel') ?>
                    </a>
                </p>
            </span>
          <?php endforeach; ?>
        </div>
    </div>
