<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * Blueowl custom
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;
get_header('shop');
?>
  <div class="breadcrumb-box"><?php
if (function_exists('yoast_breadcrumb') && !is_front_page()) {
	yoast_breadcrumb('<p id="breadcrumbs" class="alignfull">', '</p>');
}
?></div><?php

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
do_action('woocommerce_before_main_content');

$term = get_queried_object();
$cat_term_color = get_term_meta($term->term_id, 'color_picker', true);
$thumbnail_id = get_term_meta($term->term_id, 'category_banner_image', true);
$image = wp_get_attachment_image_url($thumbnail_id, 'productImage');

add_filter('category_css_class', 'add_category_parent_css', 10, 4);

$order_images = [
	[
		'images' => array(
			'class_text' => array(
				'icon-circle',
				'icon-dots-vertical',
				'icon-trangle',
			),
		),
		'class' => 'set_0'
	],
	[
		'images' => array(
			'class_text' => array(
				'icon-dots-vertical',
				'icon-half',
				'icon-trangle',
			),
		),
		'class' => 'set_1'
	],
	[
		'images' => array(
			'class_text' => array(
				'icon-half',
				'icon-lines',
			),
		),
		'class' => 'set_2'
	]
];

$randNum = rand(0, 2);
?>
<div class="customArchiveProduct" data-category="<?= get_queried_object()->slug ?>">

<?php if (($term->description || $cat_term_color || $image) && !is_shop()) : ?>
  <div class="customArchiveProduct__banner <?= $order_images[$randNum]['class']; ?>"
       style="background: <?= $cat_term_color; ?>">
    <div class="customArchiveProduct__bannerWrapper alignfull">
      <div class="customArchiveProduct__bannerText">
				<?php if($order_images[$randNum]['images']['class_text']) : ?>
					<?php foreach ($order_images[$randNum]['images']['class_text'] as $element) : ?>
            <span class="<?=$element;?>"></span>
					<?php endforeach; ?>
				<?php endif; ?>
        <div>
					<?= $term->description; ?>
        </div>
      </div>
      <div class="customArchiveProduct__bannerImage" style="background-image: url(<?php //$image; ?>)">
        <img src="<?= $image; ?>" alt="">
      </div>
    </div>

    </div>
		<?php endif; ?>
    <div class="alignfull">
      <div class="customArchiveProduct__filters" id="customArchiveProduct__filters">
				<?= get_sidebar('filter'); ?>
      </div>
    </div>
    <div class="alignwide">
      <div id="product_loop__wrapper" class="customArchiveProduct__loop">
				<?php
				if (woocommerce_product_loop()) {
					
					/**
					 * Hook: woocommerce_before_shop_loop.
					 *
					 * @hooked woocommerce_output_all_notices - 10
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
					remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
					do_action('woocommerce_before_shop_loop');
					
					woocommerce_product_loop_start();
					
					if (wc_get_loop_prop('total')) {
						while (have_posts()) {
							the_post();
							
							/**
							 * Hook: woocommerce_shop_loop.
							 */
							do_action('woocommerce_shop_loop');
							
							wc_get_template_part('content', 'product');
						}
					}
					
					woocommerce_product_loop_end();
					
					/**
					 * Hook: woocommerce_after_shop_loop.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action('woocommerce_after_shop_loop');
				} else {
					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action('woocommerce_no_products_found');
				}
				?>
      </div>
      <!--		<div id="pagination" class="customArchiveProduct__pagination button__muunel">-->
			<?//=__('Load more', 'muunel')?><!--</div>-->
    </div>
    </div>
		<?php
		/**
		 * Hook: woocommerce_after_main_content.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
		
		get_footer('shop');
