<?php
/**
 * Reset filters button
 *
 * @author  YITH
 * @package YITH\AjaxProductFilter\Templates\Filters
 * @version 4.0.0
 */

/**
 * Variables available for this template:
 *
 * @var $preset YITH_WCAN_Preset
 */

if ( ! defined( 'YITH_WCAN' ) ) {
	exit;
} // Exit if accessed directly

$button_class = apply_filters( 'yith_wcan_filter_reset_button_class', 'btn btn-primary yith-wcan-reset-filters reset-filters' );
?>

<button class="<?php echo esc_attr( $button_class ); ?>">
    <span>
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M20.6655 2.95973C20.401 2.36723 19.8086 1.98967 19.1598 2.00022C18.511 2.01076 17.9311 2.40736 17.6861 3.00815L14.647 10.6802C13.8147 10.3631 12.873 10.6405 12.3454 11.3581L11.2281 12.8925C9.44356 14.3927 7.28932 15.3869 4.98988 15.7714C4.83704 15.7927 4.7132 15.9061 4.6785 16.0565C4.6438 16.2068 4.70543 16.363 4.83346 16.4492C4.85208 16.4492 6.2189 17.384 8.00285 18.505C8.74771 18.9668 9.54099 19.4622 10.3566 19.9426C11.6641 20.7421 13.0185 21.4621 14.4124 22.099C14.4422 22.1179 14.4749 22.1318 14.5092 22.14C15.0829 22.4227 15.7043 22.5958 16.3416 22.6502C16.6228 22.6789 16.8976 22.5536 17.0604 22.3224C19.1534 18.9408 18.7363 16.2704 18.6432 15.8012L18.8704 13.939C18.9774 13.0543 18.4806 12.2069 17.6563 11.8683L20.6953 4.19248C20.8487 3.79377 20.838 3.35057 20.6655 2.95973Z" stroke="black" stroke-width="1.5"/>
            <path d="M14.647 10.6802C13.8147 10.3632 12.873 10.6405 12.3454 11.3581L11.2281 12.8925C9.44356 14.3927 7.28932 15.3869 4.98988 15.7714C4.83704 15.7927 4.7132 15.9061 4.6785 16.0565C4.6438 16.2069 4.70543 16.363 4.83346 16.4492C4.85208 16.4492 6.2189 17.384 8.00285 18.505C8.74771 18.9668 9.54099 19.4622 10.3566 19.9426C11.6641 20.7421 13.0185 21.4622 14.4124 22.099C14.4422 22.118 14.4749 22.1318 14.5092 22.14C15.0829 22.4227 15.7043 22.5958 16.3416 22.6502C16.6228 22.6789 16.8976 22.5536 17.0604 22.3225C19.1534 18.9408 18.7363 16.2704 18.6432 15.8012L18.8704 13.939C18.9774 13.0543 18.4806 12.2069 17.6563 11.8683L14.647 10.6802Z" stroke="black"/>
            <path d="M11.5 12.5L18.5 15.5" stroke="black" stroke-width="1.5"/>
            <path d="M4 21.5C4.66667 21.1667 5.33333 21 6 21C6.66667 21 7.33333 21.3333 8 22" stroke="black" stroke-width="1.5" stroke-linecap="round"/>
            <path d="M7.5 18L11 16.5" stroke="black" stroke-width="1.5" stroke-linecap="round"/>
            <path d="M13.5 21.5C13.857 21.2979 14.1903 20.9645 14.5 20.5C14.8097 20.0355 15.143 19.3688 15.5 18.5" stroke="black" stroke-width="1.5" stroke-linecap="round"/>
            <path d="M1 18L4 18.5" stroke="black" stroke-width="1.5" stroke-linecap="round"/>
        </svg>
        <?php echo esc_html( apply_filters( 'yith_wcan_filter_button', _x( 'Clear All Filters', '[FRONTEND] Reset button for preset shortcode', 'yith-woocommerce-ajax-navigation' ) ) ); ?>
    </span>
</button>
