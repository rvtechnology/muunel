<?php

// include PHP files
require get_template_directory() . '/includes/metaboxes.php';
require get_template_directory() . '/includes/acfprosettings.php';
require get_template_directory() . '/includes/widgets.php';
require get_template_directory() . '/includes/enqueue.php';
require get_template_directory() . '/includes/security.php';
require get_template_directory() . '/includes/custom-endpoints.php';
require get_template_directory() . '/includes/woocommerce.php';
require get_template_directory() . '/includes/acf_register_block.php';

// theme support
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'align-wide' );
add_image_size('fullHD', 1920); // For: wp_get_attachment_image_src($image_id, $size);
add_image_size('smallThumbnail', 50);
add_image_size('productImage', 750);
add_image_size('productThumbnail', 75);
add_image_size('productCartThumbnail', 400);
add_image_size('categoryImage', 280);

// register menus
register_nav_menus( array(
	'header_menu' => __( 'Header Menu' ),
	// 'menu_cat' => __( 'Blog Menu Kategorie' ),
));

// Gutenberg styles editor
function muunel_gutenberg_scripts() {
	wp_enqueue_script('js-editor', get_stylesheet_directory_uri() . '/assets/js/editor.js', array( 'wp-blocks', 'wp-dom' ), filemtime( get_stylesheet_directory() . '/assets/js/editor.js' ), true);
}
add_action( 'enqueue_block_editor_assets', 'muunel_gutenberg_scripts' );

add_filter('site-reviews/config/forms/review-form', function ($config) {
    $config['recommend'] = [
        'label' => __('Would you recommend Muunel to a Friend?', 'muunel'),
        'options' => [
            '1' => __('1', 'muunel'),
            '2' => __('2', 'muunel'),
            '3' => __('3', 'muunel'),
            '4' => __('4', 'muunel'),
            '5' => __('5', 'muunel'),
            '6' => __('6', 'muunel'),
            '7' => __('7', 'muunel'),
            '8' => __('8', 'muunel'),
            '9' => __('9', 'muunel'),
            '10' => __('10', 'muunel')
        ],
        'type' => 'radio',
    ];
    return $config;
});

add_filter('acf/location/rule_values/post_type', 'acf_location_rule_values_Post');
function acf_location_rule_values_Post($choices){
    $choices['product_variation'] = 'Product Variation';
    return $choices;
}

add_action('woocommerce_product_after_variable_attributes', function ($loop, $variation_data, $variation) {
    global $muunel_acf;
    $muunel_acf = $loop;
    add_filter('acf/prepare_field', 'acf_prepare_field_update_field_name');

    $acf_field_groups = acf_get_field_groups();
    foreach ($acf_field_groups as $acf_field_group) {
        foreach ($acf_field_group['location'] as $group_locations) {
            foreach ($group_locations as $rule) {
                if ($rule['param'] == 'post_type' && $rule['operator'] == '==' && $rule['value'] == 'product_variation') {
                    acf_render_fields($variation->ID, acf_get_fields($acf_field_group));
                    break 2;
                }
            }
        }
    }

    remove_filter('acf/prepare_field', 'acf_prepare_field_update_field_name');
}, 10, 3);

function  acf_prepare_field_update_field_name($field){
    global $muunel_acf;
    $field['name'] = preg_replace('/^acf\[/', "acf[$muunel_acf][", $field['name']);
    return $field;
}

function muunel_admin_head_post(){
    global $post_type;
    if ($post_type === 'product') {
        wp_register_script(
            'muunel-acf-variation',
            get_stylesheet_directory_uri() . '/assets/js/acf-variation.js',
            array(
                'jquery-core',
                'jquery-ui-core'
            ),
            '1.1.0',
            true
        );

        wp_enqueue_script('muunel-acf-variation');

    }
}
add_action('admin_head-post.php', 'muunel_admin_head_post');
add_action('admin_head-post-new.php',  'muunel_admin_head_post');
add_action( 'woocommerce_save_product_variation', function( $variation_id, $i = -1 ) {
  if ( ! empty( $_POST['acf'] ) && is_array( $_POST['acf'] ) && array_key_exists( $i, $_POST['acf'] ) && is_array( ( $fields = $_POST['acf'][ $i ] ) ) ) {
      foreach ( $fields as $key => $val ) {
          update_field( $key, $val, $variation_id );
      }
  }
}, 10, 2 );

/*
 * Set post views count using post meta
 */
function setPostViews($postID) {
    $countKey = 'post_views_count';
    $count = get_post_meta($postID, $countKey, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $countKey);
        add_post_meta($postID, $countKey, '0');
    }else{
        $count++;
        update_post_meta($postID, $countKey, $count);
    }
}

add_action( 'admin_init', 'disable_autosave' );
function disable_autosave() {
    wp_deregister_script( 'autosave' );
}

/**
 * Get Instagram media on WordPress using the current Instagram (Facebook) API
 *
 * @param $token // Info on how to retrieve the token: https://www.gsarigiannidis.gr/instagram-feed-api-after-june-2020/
 * @param $user // User ID can be found using the Facebook debug tool: https://developers.facebook.com/tools/debug/accesstoken/
 * @param int $limit // Add a limit to prevent excessive calls.
 * @param string $fields // More options here: https://developers.facebook.com/docs/instagram-basic-display-api/reference/media
 * @param array $restrict // Available options: IMAGE, VIDEO, CAROUSEL_ALBUM
 *
 * @return array|mixed // Use it like that (minimal example): get_instagram_media(TOKEN, USER_ID);
 */
function get_instagram_media(
    $token,
    $user,
    $limit = 10,
    $fields = 'media_url,permalink,media_type,caption',
    $restrict = ['IMAGE']
) {
    // The request URL. see: https://developers.facebook.com/docs/instagram-basic-display-api/reference/user
    $request_url = 'https://graph.instagram.com/' . $user . '?fields=media&access_token=' . $token;

    // We use transients to cache the results and fetch them once every hour, to avoid bumping into Instagram's limits (see: https://developers.facebook.com/docs/graph-api/overview/rate-limiting#instagram-graph-api)
    $output = get_transient('instagram_feed_' . $user); // Our transient should have a unique name, so we pass the user id as an extra precaution.
    if (false === ($data = $output) || empty($output)) {
        // Prepare the data variable and set it as an empty array.
        $data = [];
        // Make the request
        $response      = wp_safe_remote_get($request_url);
        $response_body = '';
        if (is_array($response) && !is_wp_error($response)) {
            $response_body = json_decode($response['body']);
        }
        if ($response_body && isset($response_body->media->data)) {
            $i = 0;
            // Get each media item from it's ID and push it to the $data array.
            foreach ($response_body->media->data as $media) {
                if ($i >= $limit) {
                    break;
                }
                $request_media_url = 'https://graph.instagram.com/' . $media->id . '?fields=' . $fields . '&access_token=' . $token;
                $media_response    = wp_safe_remote_get($request_media_url);
                if (is_array($media_response) && !is_wp_error($media_response)) {
                    $media_body = json_decode($media_response['body']);
                }
                if (in_array($media_body->media_type, $restrict, true)) {
                    $data[] = $media_body;
                }
                $i++;
            }
        }
        // Store the data in the transient and keep if for an hour.
        set_transient('instagram_feed_' . $user, $data, MINUTE_IN_SECONDS);

        // Refresh the token to make sure it never expires (see: https://developers.facebook.com/docs/instagram-basic-display-api/guides/long-lived-access-tokens#refresh-a-long-lived-token)
        wp_safe_remote_get('https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=' . $token);
        $output = $data;
    }

    return $output;
}

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
	foreach( $items as $item ) {
		$icon = get_field('category_image', $item);
        $class = $item->classes;
		
		if( $icon ) {
            $item->title = '<img class="icon" src="'.wp_get_attachment_image_src($icon, 'productCartThumbnail')[0].'" alt="category-image"><span>'.$item->title.'</span>';
            array_push($item->classes, 'menu-item-has-icon');
        }
	}
	
	return $items;
}
?>