<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php wp_title('|', true, 'right'); ?></title>
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
  <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

  <?php wp_head(); ?>

  <?php if ( is_admin_bar_showing() ) {?>
    <style>
      @media (min-width: 0px) {
          #header { top: 46px !important; }
      }
      @media (min-width: 768px) {
          #header { top: 32px !important; }
      }
    </style>
  <?php
  } else { ?>
    <style>
    #header { top: 0 !important; }
    </style>
  <?php } ?>
  <?php
    $main_setting = get_field('main_setting', 'options');
    $margintop = "71px";
    if($main_setting['is_label_on']){
      $margintop = "101px";
    }
  ?>
  <style type="text/css" media="print">
    .no-print { display: none; }
  </style>
  <?php if($main_setting['google_tags']): ?>
    <?php foreach($main_setting['google_tags'] as $tag) : ?>
      <?=$tag['script_head']?>
    <?php endforeach; ?>
  <?php endif; ?>
</head>
<body <?php body_class(); ?> style="position:relative;margin-top: <?=$margintop?>" id="<?=get_queried_object()->post_name;?>">
<?php global $woocommerce; ?>
<?php if($main_setting['google_tags']): ?>
  <?php foreach($main_setting['google_tags'] as $tag) : ?>
    <?=$tag['script_body']?>
  <?php endforeach; ?>
<?php endif; ?>
<header class="header no-print">
  <?php if($main_setting['is_label_on']): ?>
    <div class="header__infolabel"><div class="header__infolabel--text alignfull"><p><?=$main_setting['main_menu_label']?></p></div></div>
  <?php endif; ?>
  <div class="header__content alignfull">
    <nav class="header__content--menu">
      <?php wp_nav_menu(array(
          'menu'              => 'header_menu',
          'theme_location'    => 'header_menu',
          'depth'             => 2,
          'menu_class'        => 'menu',
          'container'         => false,
      ));?>
      <div id="navMenu__trigger" class="burger">
          <div class="burger__inner"></div>
      </div>
    </nav>
    <div class="header__content--logo">
      <a href="<?php echo get_home_url(); ?>"><img src="<?=$main_setting['logo']['url']?>" alt="<?=esc_attr($main_setting['logo']['title'])?>"></a>
    </div>
    <div class="header__content--shopButtons">
        <a id="loginModalTrigger" class="shopButton" <?= is_user_logged_in() ? 'href="'.get_permalink(get_option('woocommerce_myaccount_page_id')).'"' : ''; ?>>
          <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="12.2" cy="6.6" r="3.6" stroke="#0B0B0A" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M19.2575 18.6677C19.4279 19.5116 18.847 20.2762 18.0039 20.45C16.7775 20.7028 14.786 21 12.2 21C9.8681 21 7.79512 20.6905 6.47932 20.4345C5.5984 20.2631 4.97107 19.4704 5.15804 18.5926C5.85094 15.3398 8.74055 12.9 12.2 12.9C15.686 12.9 18.5934 15.3775 19.2575 18.6677Z" stroke="#0B0B0A" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
          </svg>
        </a>
        <a id="miniCartTrigger" class="shopButton shopButton--cart" href="<?php echo wc_get_cart_url(); ?>">
          <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M2 10H22L19 21H5L2 10Z" stroke="black" stroke-width="1.5" stroke-linecap="round"/>
            <path d="M12 18V13" stroke="black" stroke-width="1.5"/>
            <path d="M16 18L17.5 13" stroke="black" stroke-width="1.5"/>
            <path d="M8 18L6.5 13" stroke="black" stroke-width="1.5"/>
            <path d="M5 10L9 4.5" stroke="black" stroke-width="1.5"/>
            <path d="M19 10L15 4.5" stroke="black" stroke-width="1.5"/>
          </svg>
          <?php if($woocommerce->cart->cart_contents_count > 0 && !is_cart()) : ?>
            <span class="shopButton--count"><?php echo $woocommerce->cart->cart_contents_count; ?></span>
          <?php endif; ?>
        </a>
      </div>
  </div>
</header>