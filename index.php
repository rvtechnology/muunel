<?php get_header(); ?>

<?php
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb( '<p id="breadcrumbs" class="alignfull">','</p>' );
	}
?>
<div class="pageWrapper alignfull">
	<section class="trendArticles" id="<?= $id; ?>">
		<div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>">
			<?php
				$popularpost = new WP_Query( array(
					'meta_key' => 'post_views_count',
					'orderby' => 'meta_value_num',
					'order' => 'DESC',
					'posts_per_page' => 8
				));
			?>
			<h1 class="splide__slide__content"><?= __("Trending Articles", "muunel"); ?></h1>
			<div class="splide trendArticles__sliders" >
				<div class="splide__track">
					<ul class="splide__list">
					<?php while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>
					<?php global $post; ?>

						<li class="splide__slide" style="background-image: url('<?php the_post_thumbnail_url('productImage');?>')">
							<a href="<?php the_permalink( );?>">
								<h4><?= get_the_title(); ?></h4>
								<p><?= wp_trim_words(get_the_excerpt(), 60, '[...]'); ?></p>
								<div class="row">
									<p class="date"><?=get_the_date('d M Y', $post);?></p>
									<span class="read-more"><?= __("Read more", "muunel"); ?></span>
								</div>
							</a>
						</li>

					<?php endwhile; ?>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<h3><?=__('New Article', 'muunel');?></h3>
	<section id="blogLoop__wrapper" class="blogLoop__wrapper" data-category="<?=get_queried_object()->slug?>"></section>
	<div id="pagination" class="blog__nav">
		<div class="prev-page"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 5L3 12L10 19" stroke="black" stroke-width="1.5"/><path d="M3.5 12H23" stroke="black" stroke-width="1.5"/></svg><span><?=__('Prev', 'muunel')?></span></div>
		<ul class="pages"></ul>
		<div class="next-page"><span><?=__('Next', 'muunel')?></span><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 5L3 12L10 19" stroke="black" stroke-width="1.5"/><path d="M3.5 12H23" stroke="black" stroke-width="1.5"/></svg></div>
	</div>
</div>
<?php get_footer(); ?>
