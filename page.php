<?php get_header(); ?>
<div class="breadcrumb-box">
	<?php
		if ( function_exists('yoast_breadcrumb') && !is_front_page() && !is_checkout() && !is_cart() && !is_account_page()) {
			yoast_breadcrumb( '<p id="breadcrumbs" class="alignfull">','</p>' );
		}
	?>
	<?php if(get_field('is_printing')): ?>
	  <div class="print-page no-print">
		<button type="button" id="cart-printing">
		  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M21 20V10H3V20" stroke="#0B0B0A" stroke-width="1.5"/>
			<path d="M17 10V4H7V10" stroke="#0B0B0A" stroke-width="1.5"/>
			<line x1="6" y1="18.25" x2="18" y2="18.25" stroke="#0B0B0A" stroke-width="1.5"/>
			<line x1="10" y1="14.25" x2="14" y2="14.25" stroke="#0B0B0A" stroke-width="1.5"/>
		  </svg>
		  <?=__('Print', 'muunel');?>
		</button>
	  </div>
	<?php endif; ?>
	<?php if(get_post_meta( get_the_ID(), 'sharing_disabled', true ) == ""):?>
	<button class="customPage--shareButton no-print" id="shareModalBtn">
		<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M18 19L6 12L18 5" stroke="black" stroke-width="1.5"/>
			<circle cx="18" cy="5" r="3" fill="white" stroke="black" stroke-width="1.5"/>
			<circle cx="6" cy="12" r="3" fill="white" stroke="black" stroke-width="1.5"/>
			<circle cx="18" cy="19" r="3" fill="white" stroke="black" stroke-width="1.5"/>
		</svg>
	</button>
	<?php endif; ?>
</div>

<div class="pageWrapper">

	<?php if (have_posts()) : ?>

	<?php while (have_posts()) : the_post(); ?>

		<?php the_content('Read the rest of this entry &raquo;'); ?>

	<?php endwhile; ?>

	<?php else: ?>

	<h2 class="text-center">Not Found</h2>
	<p class="text-center">Sorry, but you are looking for something that isn't here.</p>
	<?php get_search_form(); ?>

	<?php endif; ?>
</div>
<?php get_footer(); ?>
