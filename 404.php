<?php get_header(); ?>
<div class="pageWrapper">
	<div class="page404">
		<div class="page404__wrapper">
			<h1><?= __('Error 404', 'muunel'); ?></h1>
			<h4><?=__('Page not found...', 'muunel');?></h4>
			<p><?=__('Unfortunately, we couldn\'t find page you are looking for', 'muunel');?></p>
			<a href="<?php echo get_home_url(); ?>" class="button__muunel"><?=__('Back to home page', 'muunel')?></a>
		</div>
	</div>
	</div>
<?php get_footer(); ?>
