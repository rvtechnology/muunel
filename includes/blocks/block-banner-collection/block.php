<?php
$id = 'blockBannerCollection-' . $block['id'];
$buttonWoman = get_field('button_woman');
$buttonMan = get_field('button_man');
$image = get_field('image');
?>
<section class="section blockBannerCollection" id="<?= $id; ?>" >
    <div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?> flex" style="background-color:<?php echo get_field('box_color');?>"> 
        <div class="cute-1"></div>
        <img src="<?= wp_get_attachment_image_src($image, 'productImage')[0];?>" alt="collaction-image">
        <div class="content">
            <?= get_field('content');?>
            <div class="links flex">
                <?php if($buttonMan): ?><a href="<?= $buttonMan['url'];?>"><?= $buttonMan['title'];?></a><?php endif; ?>
                <?php if($buttonWoman): ?><a href="<?= $buttonWoman['url'];?>"><?= $buttonWoman['title'];?></a><?php endif; ?>
                <div class="cute-2"></div>
            </div>
        </div>
    </div>
</section>