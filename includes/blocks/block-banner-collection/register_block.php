<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-banner-collection',
            'title'				=> __('Banner Collection'),
            'description'		=> __('Banner Collection'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Banner Collection', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}