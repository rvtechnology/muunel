<?php
$id = 'blockBannerHomepagePeople-' . $block['id'];
?>
<section class="section blockBannerHomepagePeople" id="<?= $id; ?>">
    <div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?> flex">
        <a href="<?= get_field('woman_category_link'); ?>">
            <div class="blockBannerHomepagePeople__photobox womanbox">
                <img class="face shadow" src="<?= get_field('woman_image_shadow'); ?>')"/>
                <img class="face" src="<?= get_field('woman_image'); ?>')"/>
                <h3><?= get_field('woman_category_name'); ?></h3>
            </div>
        </a>
        <a href="<?= get_field('man_category_link'); ?>">
            <div class="blockBannerHomepagePeople__photobox manbox">
                <img class="face shadow" src="<?= get_field('man_image_shadow'); ?>')"/>
                <img class="face" src="<?= get_field('man_image'); ?>')"/>
                <h3><?= get_field('man_category_name'); ?></h3>
            </div>
        </a>
    </div>
</section>