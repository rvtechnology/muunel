<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-banner-homepage-people',
            'title'				=> __('Banner Homepage People'),
            'description'		=> __('Banner Homepage People'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Banner Homepage', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}