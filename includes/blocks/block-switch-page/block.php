<?php
$id = 'blockSwitchPage-' . $block['id'];
?>
<section class="section blockSwitchPage <?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>" id="<?= $id; ?>">
    <div class="blockSwitchPage__change">
        <?php if(get_field('repeater')): ?>
            <?php foreach(get_field('repeater') as $item){ ?>
                <a href="<?=$item['link']['url']?>" class="btn-change <?php if($item['is_active']){ echo 'active'; } ?>"><?=$item['link']['title']?></a>
            <?php } ?>
        <?php endif; ?>
    </div>
</section>