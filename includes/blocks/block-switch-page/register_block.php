<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-switch-button',
            'title'				=> __('Switch Page Button'),
            'description'		=> __('Switch Page Button'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Switch Page Button', 'Switch Button', 'blueowl' ),
            'mode'				=> 'edit',
        )
    );
}