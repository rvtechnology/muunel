<?php
$id = 'policyTerms-' . $block['id'];
$terms_and_policy = get_field('repeater');
?>
<section class="section policyTerms <?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>" id="<?= $id; ?>">
    <div class="policyTerms__container">
        <nav class="policyTerms__container--nav">
            <ul>
                <?php foreach($terms_and_policy as $item){ ?>
                    <li><a class="paragraph-4" href="#<?=sanitize_title_with_dashes($item['title'])?>"><?=$item['title']?></a></li>
                <?php } ?>
            </ul>
        </nav>
        <div class="policyTerms__container--content">
            <?php foreach($terms_and_policy as $item){ ?>
                <div class="policyTerms__container--content__item">
                    <h4 id="<?=sanitize_title_with_dashes($item['title'])?>"><?=$item['title']?></h4>
                    <?=$item['content']?>
                </div>
            <?php } ?>
        </div>
    </div>
</section>