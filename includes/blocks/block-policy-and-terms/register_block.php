<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-policy-and-terms',
            'title'				=> __('Policy & Terms'),
            'description'		=> __('Policy & Terms'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Policy & Terms', 'Policy', 'Terms', 'blueowl' ),
            'mode'				=> 'edit',
        )
    );
}