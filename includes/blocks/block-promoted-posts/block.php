<?php
$id = 'blockPromotedPosts-' . $block['id'];
?>
<section class="section blockPromotedPosts <?php echo get_field('style_changer');?>" id="<?= $id; ?>">
    <div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?> flex">
        <?php if (get_field('header')): ?>
            <h4 class="heading"><?= get_field('header'); ?></h4>
        <?php endif ?>
        <div class="posts-container">
            <?php $pposts = get_field('promoted_posts'); ?>
            <?php if($pposts): ?>
                <?php foreach ( $pposts as $item ) : ?>
                    <a href="<?php echo $item['single_post']->guid; ?>" class="post-box">
                        <img src="<?php echo get_the_post_thumbnail_url($item['single_post']->ID, 'productImage');?>" alt="<?php echo $item['single_post']->post_name;?>">
                        <p><?php echo $item['single_post']->post_title;?></p>
                    </a>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>