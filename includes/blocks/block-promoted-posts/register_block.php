<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-promoted-posts',
            'title'				=> __('Promoted Posts'),
            'description'		=> __('Promoted Posts'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Promoted Posts', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}