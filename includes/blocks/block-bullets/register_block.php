<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-bullets',
            'title'				=> __('Bullets'),
            'description'		=> __('Bullets'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Bullets', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}