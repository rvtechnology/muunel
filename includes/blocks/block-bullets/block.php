<?php
$id = 'blockBullets-' . $block['id'];
?>
<section class="section blockBullets" id="<?= $id; ?>">
    <div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?> blockBullets__content">
        <?php
        foreach(get_field('bullets') as $item_id => $item){
            ?>
            <div class="blockBullets__content--item">
                <div class="blockBullets__content--item__icon" type="<?=$item_id+1?>"><img src="<?=wp_get_attachment_image_src($item['icon'], 'smallThumbnail')[0]?>" alt="bullets-icon-image"></div>
                <h5 class="blockBullets__content--item__title"><?=$item['title']?></h5>
                <div class="blockBullets__content--item__desc paragraph-1"><?=$item['desc']?></div>
            </div>
            <?php
        }
        ?>
    </div>
</section>