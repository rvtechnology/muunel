<?php
$id = 'blockBannerHomepage-' . $block['id'];
?>
<section class="section blockBannerHomepage <?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>" id="<?= $id; ?>">
    <div class="blockBannerHomepage__content">
        <?php if(get_field('video_url')): ?>
        <video class="video-js" poster="<?=wp_get_attachment_image_src(get_field('video_poster'), 'fullHD')[0];?>" id="bannerHomepageVideo" loop muted autoplay="true" data-url="<?=get_field('video_url');?>" data-tablet-url="<?=get_field('tablet_video_url');?>" data-mobile-url="<?=get_field('mobile_video_url');?>"></video>
        <?php endif; ?>
        <?php if(get_field('links')): ?>
            <div class="blockBannerHomepage__content--links">
                <?php foreach(get_field('links') as $link): ?>
                    <a class="button__muunel" href="<?=$link['button']['url']?>"><?=$link['button']['title']?></a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</section>