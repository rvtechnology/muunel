import Hls from 'hls.js';
import videojs from 'video.js';
const blockBannerHomepage = document.querySelector('.blockBannerHomepage');

export const bannerHomepage = () => {
    if(blockBannerHomepage){
        let video = blockBannerHomepage.querySelector('video');
        if(video){
          let url = video.getAttribute('data-url');
          if(window.innerWidth <= 992) url = video.getAttribute('data-tablet-url');
          if(window.innerWidth <= 768) url = video.getAttribute('data-mobile-url');
          if (Hls.isSupported()) {
            if(video.canPlayType('application/vnd.apple.mpegurl')){
              let source = document.createElement('source');
              source.src = url; source.type = "application/x-mpegURL";
              video.appendChild(source);
              videojs(video, {
                autoplay: true,
                controls: false,
                loop: true,
                muted: true,
                poster: video.getAttribute('poster')
              })
              video.setAttribute('playsinline', '');
            } else {
              var hls = new Hls();
              hls.loadSource(url);
              hls.attachMedia(video);
              hls.on(Hls.Events.MANIFEST_PARSED, function() {
                let player = videojs(video, {
                  autoplay: true,
                  controls: false,
                  loop: true,
                  muted: true,
                  preload: 'auto'
                })
                player.on('ready', function(){
                  this.play();
                })
              });
            }
          } else{
            let source = document.createElement('source');
            source.src = url; source.type = "application/x-mpegURL";
            video.appendChild(source);
            videojs(video, {
              autoplay: true,
              controls: false,
              loop: true,
              muted: true
            })
            video.setAttribute('playsinline', '');
          }
        }
    }
}