<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-banner-homepage',
            'title'				=> __('Main Page Video'),
            'description'		=> __('Main Page Video'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Main Page', 'video', 'home', 'page', 'banner', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}