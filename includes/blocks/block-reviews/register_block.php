<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-reviews',
            'title'				=> __('Review Carousel'),
            'description'		=> __('Review Carousel'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Review Carousel', 'Review', 'Carousel','blueowl' ),
            'mode'				=> 'edit',
        )
    );
}