import Splide from '@splidejs/splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';

export function initReviewCarousel() {
    document.addEventListener( 'DOMContentLoaded', function () {
        const blocks = document.querySelectorAll('.reviewCarousel__sliders');
        if (blocks) {
            blocks.forEach(block=>{
                new Splide(block, {
                    arrows: true,
                    type: "loop",
                    perPage: 4
                }).mount();
            })
        }
    })
}