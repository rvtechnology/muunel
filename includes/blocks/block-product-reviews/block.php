<?php
$id = 'productReviews-' . $block['id'];
?>
<section class="section productReviews glsr" id="<?= $id; ?>">
    <div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>">
        <h2 class="splide__slide__content"><i class="triangle"></i><?= get_field('header') ?></h2>
        <h6 class="splide__slide__desc"><?= get_field('description') ?></h6>
        <div class="cute-1"></div>
        <?php 
        // $reviews = apply_filters('glsr_get_reviews', []);
        $reviews = apply_filters('glsr_get_reviews', [], ['assigned_posts' => 'post_id']);
        if($reviews): ?>
        <div class="splide productReviews__sliders" >
            <div class="splide__track">
			<?php global $wp_query; $productId = $wp_query-> post->ID; ?>
                <ul class="splide__list">
                <?php 
                    foreach ($reviews as $review) {
                        // Use the $review object to build your slide html.
                        // You can use the debug function to print the review object to the page like this:
                        // apply_filters('glsr_debug', null, $review);
                    
                        // You can render the star rating like this:
                        $starsHtml = $review->rating();
                        $title = $review->name;
                        $content = $review->content;
                    ?>
                    <li class="splide__slide">
                        <p class="heading"><?= $title; ?></p>
                        <div class="rating"><?= $starsHtml; ?></div>
                        <p><?= $content; ?></p>
                    </li>
                    <?php
                }
                ?>
                </ul>
            </div>
            <div class="splide__arrows">
                <button class="splide__arrow splide__arrow--prev"></button>
                <button class="splide__arrow splide__arrow--next"></button>
            </div>
        </div>
        <?php endif;?>
        <div class="button-box">
            <button class="button__muunel" id="reviewModalBtn"><?=__('Review Item', 'muunel')?></button>
        </div>
    </div>
</section>

	


