<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-product-reviews',
            'title'				=> __('Product Reviews'),
            'description'		=> __('Product Reviews'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Product Reviews', 'blueowl' ),
            'mode'				=> 'edit',
        )
    );
}