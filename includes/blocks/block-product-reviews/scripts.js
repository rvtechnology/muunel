import Splide from '@splidejs/splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';

export function productReviewsScripts() {
    document.addEventListener( 'DOMContentLoaded', function () {
        const blocks = document.querySelectorAll('.productReviews__sliders');
        if (blocks) {
            blocks.forEach(block=>{
                new Splide(block, {
                    cover: true,
                    arrows: true,
                    type: "loop",
                    perPage: 2,
                    perMove: 1,
                    gap: "20px",
                    breakpoints: {
                        1110: {
                            perPage: 2,
                        },
                        768: {
                            perPage: 1,
                        },
                        480: {
                            perPage: 1,
                        },
                    }
                }).mount();
            })
        }
    })
}