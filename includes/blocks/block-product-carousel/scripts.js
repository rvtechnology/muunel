import Splide from '@splidejs/splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';

export function productCarouselScripts() {
    document.addEventListener( 'DOMContentLoaded', function () {
        const blocks = document.querySelectorAll('.productCarousel__sliders');
        if (blocks) {
            blocks.forEach(block=>{
                new Splide(block, {
                    cover: true,
                    arrows: false,
                    type: "loop",
                    perPage: 4,
                    perMove: 1,
                    gap: "20px",
                    breakpoints: {
                        1110: {
                            perPage: 3,
                        },
                        768: {
                            perPage: 2,
                        },
                        480: {
                            perPage: 1,
                            fixedWidth: '300px'
                        },
                    }
                }).mount();
            })
        }
    })
}