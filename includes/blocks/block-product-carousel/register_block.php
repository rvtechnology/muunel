<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-product-carousel',
            'title'				=> __('Product carousel'),
            'description'		=> __('Product carousel'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Product carousel', 'blueowl' ),
            'mode'				=> 'edit',
        )
    );
}