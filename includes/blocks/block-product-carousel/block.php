<?php
$id = 'productCarousel-' . $block['id'];
$productCarouselArray = get_field('repeater');
?>
<section class="section productCarousel" id="<?= $id; ?>">
    <div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>">
        <?php if(get_field('text')): ?>
        <h3 class="splide__slide__content"><?= get_field('text') ?></h3>
        <?php endif; ?>
        <?php if($productCarouselArray): ?>
        <div class="splide productCarousel__sliders" >
            <div class="splide__track">
                <ul class="splide__list">
                <?php
                foreach ($productCarouselArray as $singleProduct ) :
                    if( $singleProduct["product"]) :
                        $variantID = '';
                        $variation = '';
                        $product = '';
                    ?>
                    <li class="splide__slide">
                        <?php
                            $variantID = $singleProduct["product"];
                            if (wc_get_product( $variantID )) {
                                $variation = wc_get_product( $variantID );
                            }

                            if (wc_get_product($variation->get_parent_id())) {
                                $product = wc_get_product($variation->get_parent_id());
                            }


                        ?>
                        <a class="productCarousel__single_post" href="<?php if($product != '') { echo $product->get_permalink(); }?>?attribute_pa_color=<?php if($variation != '') { echo $variation->get_attributes()['pa_color']; }?>">
                            <?php if($variation != '') : ?>
                            <div class="productCarousel__single_post__image">

                                <?=$variation->get_image();?>
                                <?php
                                    if(get_field('hover_image', $variation->get_id())){
                                        $hover = wp_get_attachment_image_src(get_field('hover_image', $variation->get_id()), 'productImage')[0];
                                        ?><img class="card_image_hover" src="<?=$hover?>" alt="product-image-hover"><?php
                                    }
                                ?>

                            </div>
                            <?php endif; ?>
                            <?php if($product != '') : ?>
                            <h4 class="productCarousel__single_post__title"><?= $product->get_title(); ?></h4>
                            <?php endif; ?>
                            <?php if($variation != '') : ?>
                            <h6><?php echo $variation->get_attribute('pa_color'); ?></h6>
                            <h5><?= get_woocommerce_currency_symbol() . $variation->get_price(); ?></h5>
                            <?php endif; ?>
                        </a>
                    </li>
                    <?php
                    endif;
                endforeach;
                ?>
                </ul>
            </div>
            <div class="splide__arrows">
                <button class="splide__arrow splide__arrow--prev"></button>
                <button class="splide__arrow splide__arrow--next"></button>
            </div>
        </div>
        <?php endif; ?>
        <div class="button-box">
            <a href="<?php echo get_field('button')['url']; ?>" class="button__muunel"><?php echo get_field('button')['title']; ?></a> 
        </div>
    </div>
</section>