export const cloudsInit = () => {
  const clouds = document.querySelectorAll('.blockProduct__bullets--item');

  if( clouds != '' ) {
    if(window.innerWidth <= 768 ){
      document.addEventListener('click', (event) => {
        clouds.forEach(item => {
          if(item.contains(event.target)) {
            (item.classList.contains('selected')) ? item.classList.remove('selected') : item.classList.add('selected');
          } else {
            item.classList.remove('selected');
          }
        })
      })
    } else {
      clouds.forEach(item => {
        item.addEventListener('mouseenter', () => {
          clouds.forEach(cloud => {
            if(cloud != item){
              cloud.classList.remove('selected');
            }
          })
          item.classList.add('selected');
        })
        item.addEventListener('mouseleave', () => {
          item.classList.remove('selected');
        })
      })
    }
  }
}