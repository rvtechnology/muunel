<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-product-bullets',
            'title'				=> __('Product Bullets'),
            'description'		=> __('Product Bullets'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Product Bullets', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}