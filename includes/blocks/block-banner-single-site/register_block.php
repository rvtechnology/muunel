<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-banner-single-site',
            'title'				=> __('Banner Single Site'),
            'description'		=> __('Banner Single Site'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Banner Single Site', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}