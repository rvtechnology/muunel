<?php
$id = 'blockBannerSingleSite-' . $block['id'];
?>
<section class="section blockBannerSingleSite <?php echo get_field('style_changer');?>" id="<?= $id; ?>">
    <div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?> flex">
        <div class="blockBannerSingleSite__content">
            <h3><?php the_field('header'); ?></h3>
            <p><?php the_field('description'); ?></p>
        </div>
        <div class="blockBannerSingleSite__photo">
            <img src="<?= get_field('photo')['url'] ?>" alt="<?= esc_attr(get_field('photo')['title']) ?>">
        </div>
    </div>
    <div class="elements element-1"></div>
    <div class="elements element-2"></div>
    <div class="elements element-3"></div>
</section>