<?php
// Desktop/mobile image
$id = 'image-' . $block['id'];
?>
<section class="image-section <?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>" id="<?= $id; ?>">
    <?php if(get_field('img_desktop')): ?>
    <img class="image-section__desktop" style="max-width:<?=get_field('max_width_desktop');?>%" src="<?=wp_get_attachment_image_src(get_field('img_desktop'), 'fullHD')[0]?>" alt="single-image-desktop">
    <?php endif; if(get_field('img_mobile')): ?>
    <img class="image-section__mobile" style="max-width:<?=get_field('max_width_mobile');?>%" src="<?=wp_get_attachment_image_src(get_field('img_mobile'), 'productImage')[0];?>" alt="single-image-mobile">
    <?php endif; ?>
</section>