<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-image',
            'title'				=> __('Single image'),
            'description'		=> __('Single image'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Image', 'single', 'desktop', 'mobile', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}