<?php
$id = 'contact-' . $block['id'];
?>
<section class="section contact" id="<?= $id; ?>">
    <div class="<?=$block['className']?>">
        <h2><?= get_field('header'); ?></h2>
        <p class="desc"><?= get_field('description'); ?></p>
        <div class="contact-box">
            <div class="content-box" style="background-color: <?= get_field('box_color'); ?>">
                <h2><?= get_field('box_heading'); ?></h2>
                <p><?= get_field('box_text'); ?></p>
                <div class="links">
                <?php
                $links = get_field('links');
                foreach ($links  as $single ){
                    $link = $single['link'];
                ?>
                
                    <a href="<?= $link['url']; ?>"><i style="background-image:url('<?= $single['icon']; ?>')"></i><?= $link['title']; ?></a>
                <?php
                }
                ?>
                </div>
                <div class="cute-1"></div>
            </div>
            <div class="form-box">
                <?php echo do_shortcode(get_field('form')); ?>
            </div>
        </div>
    </div>
</section>