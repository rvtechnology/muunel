<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-contact',
            'title'				=> __('Contact'),
            'description'		=> __('Contact'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Contact', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}