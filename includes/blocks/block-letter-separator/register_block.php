<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-letter-separator',
            'title'				=> __('Letter Separator'),
            'description'		=> __('Letter Separator'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Letter Separator', 'Separator', 'Letter', 'blueowl' ),
            'mode'				=> 'edit',
        )
    );
}