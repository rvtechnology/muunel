<?php
$id = 'blockLetterSeparator-' . $block['id'];
?>
<section class="blockLetterSeparator <?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>" data-letter="<?=esc_attr(get_field('letter'));?>" id="<?= $id; ?>">
    <span><?=get_field('letter');?></span>
</section>