<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-banner-relative',
            'title'				=> __('Banner Relative'),
            'description'		=> __('Banner Relative'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Banner Relative', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}