<?php
$id = 'blockBannerRelative-' . $block['id'];
?>
<section class="section blockBannerRelative <?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>" id="<?= $id; ?>">
    <div class="background-img" style="background-image:url(<?=wp_get_attachment_image_src(get_field('background_image'), 'fullHD')[0]; ?>)"></div>
    <div class="content-box" style="background-color:<?= get_field('box_color'); ?>">
        <?= get_field('content'); ?>
    </div>
</section>