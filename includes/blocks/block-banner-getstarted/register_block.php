<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-banner-getstarted',
            'title'				=> __('Banner Get started'),
            'description'		=> __('Banner Get started'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Banner Get started', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}