<?php
$id = 'blockBannerGetstarted-' . $block['id'];
$buttonWoman = get_field('button_woman');
$buttonMan = get_field('button_man');
$image = get_field('image');
?>
<section class="section blockBannerGetstarted" id="<?= $id; ?>">
    <div class="cute-1"></div>
	<div class="cute-2"></div>
    <div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?> blockBannerGetstarted__content">
        <img src="<?= wp_get_attachment_image_src($image, 'productImage')[0];?>" alt="getstarted-image">
        <div class="links flex">
            <?php if($buttonMan): ?><a href="<?= $buttonMan['url'];?>"><?= $buttonMan['title'];?></a><?php endif; ?>
            <?php if($buttonWoman): ?><a href="<?= $buttonWoman['url'];?>"><?= $buttonWoman['title'];?></a><?php endif; ?>
        </div>
    </div>
</section>