<?php
$id = 'blockBenefitLists-' . $block['id'];
?>
<section class="section blockBenefitLists" id="<?= $id; ?>">
    <div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?> blockBenefitLists__content">
        <h2><?= get_field('title'); ?></h2>
        <div class="content-box">
            <div class="cute-1"></div>
            <div class="cute-2"></div>
            <div class="list">
                <h4><?=get_field('heading_1');?></h4>
                <?php
                foreach(get_field('first_list') as $item){
                ?>
                    <div class="item">
                        <img src="<?=wp_get_attachment_image_src($item['icon'], 'smallThumbnail')[0]?>" alt="<?=$item['icon']['title'];?>"/>
                        <p><?=$item['desc']?></p>
                    </div>
                <?php
                }
                ?>
            </div>
            <div class="list">
                <h4><?=get_field('heading_2');?></h4>
                <?php
                foreach(get_field('sec_list') as $item){
                ?>
                    <div class="item">
                        <img src="<?=wp_get_attachment_image_src($item['icon'], 'smallThumbnail')[0]?>" alt="list-block-icon"/>
                        <p><?=$item['desc']?></p>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</section>