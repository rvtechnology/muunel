<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-benefit-lists',
            'title'				=> __('Benefit Lists'),
            'description'		=> __('Benefit Lists'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Benefit Lists', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}