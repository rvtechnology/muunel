<?php
$id = get_field('id') ? sanitize_title(get_field('id')) : 'questionAnswer-' . $block['id'];
?>
<section class="section questionAnswer" id="<?= $id; ?>">
    <div class="questionAnswer__content <?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>">
        <div class="faq-single-box">
            <p class="question"><?= get_field('question');?></p>
            <div class="answer-box">
                <p class="answer"> <?= get_field('answer');?></p>
                <div class="cute-1"></div>
            </div>
        </div>
    </div>
</section>