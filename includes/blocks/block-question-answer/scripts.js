export function questionAnswerScript() {
    let faqBox = document.querySelectorAll('.faq-single-box');
    if(faqBox){
        faqBox.forEach(box => {
            box.addEventListener('click',  () => {
                if(box.classList.contains('active')) {
                    box.classList.remove('active');
                } else {
                    box.classList.add('active');
                }
            })
        })
        window.addEventListener('DOMContentLoaded', () => {
            if(location.hash && document.querySelector(`${location.hash}`)){
                let posY = document.querySelector(`${location.hash}`).offsetTop - 110;
                window.scrollTo({
                    top: posY,
                    behavior: 'smooth'
                })
                document.querySelector(`${location.hash} .faq-single-box`).click();
            }
            window.addEventListener('hashchange', () => {
                if(location.hash && document.querySelector(`${location.hash}`)){
                    let posY = document.querySelector(`${location.hash}`).offsetTop - 110;
                    window.scrollTo({
                        top: posY,
                        behavior: 'smooth'
                    })
                    document.querySelector(`${location.hash} .faq-single-box`).click();
                }
            })
        })
    }
}