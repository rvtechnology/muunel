<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-question-answer',
            'title'				=> __('Question Answer'),
            'description'		=> __('Question Answer'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Question Answer', 'blueowl' ),
            'mode'				=> 'edit',
        )
    );
}