<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-letter-navigation',
            'title'				=> __('Letter Navigation'),
            'description'		=> __('Letter Navigation'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Letter Navigation', 'Navigation', 'Letter', 'blueowl' ),
            'mode'				=> 'edit',
        )
    );
}