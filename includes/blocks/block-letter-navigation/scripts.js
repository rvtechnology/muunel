const LetterSeparators = document.querySelectorAll('.blockLetterSeparator');
const LetterNavigationWrapper = document.querySelector('.blockLetterNavigation__content');

export const initLetterNavigation = () => {
    if(LetterSeparators && LetterNavigationWrapper){
        LetterNavigationWrapper.innerHTML = "";

        LetterSeparators.forEach(item => {
            let letter = document.createElement('span');
            letter.innerHTML = item.innerText;
            letter.classList.add('paragraph-3');
            letter.onclick = () => {
                let posY = item.offsetTop - 110;
                window.scrollTo({
                    top: posY,
                    behavior: 'smooth'
                });
            }
            LetterNavigationWrapper.appendChild(letter);
        })
    }
}
