<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-main-carousel',
            'title'				=> __('Main carousel'),
            'description'		=> __('Main carousel'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Main carousel', 'blueowl' ),
            'mode'				=> 'edit',
        )
    );
}