import Splide from '@splidejs/splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';

export function mainCarouselScripts() {
    document.addEventListener( 'DOMContentLoaded', function () {
        const blocks = document.querySelectorAll('.mainCarousel__sliders');
        if (blocks) {
            blocks.forEach(block=>{
                new Splide(block, {
                    height: "300px",
                    cover: true,
                    arrows: true,
                    type: "loop",
                    perPage: 4,
                    perMove: 1,
                    gap: "2px",
                    breakpoints: {
                        1110: {
                            perPage: 3,
                        },
                        768: {
                            perPage: 2,
                        },
                        480: {
                            perPage: 1,
                            fixedWidth: '300px'
                        },
                    }
                }).mount();
            })
        }
    })
}