<?php
$id = 'mainCarousel-' . $block['id'];
?>
<section class="section mainCarousel" id="<?= $id; ?>">
        <div class="carousel <?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>">
            <?php if(get_field('text')): ?>
            <h3 class="splide__slide__content"><i class="instagram" style="background-image: url(<?= wp_get_attachment_image_src(get_field('icon'), 'smallThumbnail')[0]; ?>)"></i><?= get_field('text') ?></h3>
            <?php endif; ?>
            <div class="splide mainCarousel__sliders" >
                <div class="splide__track">
                    <ul class="splide__list">
                    <?php
                    if(!get_field('is_instagram')):
                        if(get_field('single_banner')):
                            $mainCarouselArray = get_field('single_banner');
                            foreach ($mainCarouselArray  as $singleBanner ): ?>
                                <li class="splide__slide" style="background-image: url(<?= wp_get_attachment_image_src($singleBanner['image'], 'productImage')[0] ?>)"></li><?php
                            endforeach;
                        endif;
                    else:
                        $instagram = get_instagram_media('IGQVJVay13eTZARdDlTSWZAiQlR2ZAW1ZAMWpZAUnd2dndjQjZAmc1ZASTzZAJWUdmdmlZAcm95NTJ3YTJIYzQwSzdGOFJmY1Fud2pFVTRRQUtSX05mM1hyMENLY2p0ZAEl6ZAkRYRE9hdHFpejZA3', '17841435528232208', 12);
                        if($instagram){
                            foreach($instagram as $photo){
                                $img_path = wp_upload_dir()['basedir'] . '/muunel-insta/';
                                $image = wp_get_image_editor($photo->media_url);
                                if (!is_wp_error($image)) {
                                    $image->resize(400, 400, true);
                                    $image->save($img_path . 'muunel-insta-' . $photo->id . '.jpg');
                                }?>
                                <li class="splide__slide"><a href="<?=$photo->permalink;?>" target="_blank"><img src="<?= wp_upload_dir()['baseurl'] . '/muunel-insta/muunel-insta-' . $photo->id . '.jpg'; ?>" alt="<?= esc_attr($photo->caption); ?>"></a></li>
                            <?php }
                        }
                    endif;
                    ?>
                </ul>
            </div>
        </div>
    </div>
</section>