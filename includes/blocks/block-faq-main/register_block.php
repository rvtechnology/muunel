<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-faq-main',
            'title'				=> __('FAQ Main'),
            'description'		=> __('FAQ Main'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'FAQ Main', 'FAQ', 'Main', 'blueowl' ),
            'mode'				=> 'edit',
        )
    );
}