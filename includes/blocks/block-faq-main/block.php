<?php
$id = 'faqMain-' . $block['id'];
$title_array = [];
$content_array = [];
$content_index = 0;
$category_single = get_field('single');
foreach($category_single as $key => $single){
    $single_title = (object)[
        'id'=> sanitize_title( $single['title_category'], '', 'save' ),
        'title'=> $single['title_category']
    ];
    $title_array[] = $single_title ;
    foreach ($single['boksy'] as $single_box){
        $single_box = (object) [
            'id'=> sanitize_title( $single['title_category'], '', 'save' ),
            'title'=> $single_box['title'],
            'desc'=> $single_box['desc'],
            'index'=> $content_index
        ];
        $content_array[] = $single_box;
        $content_index++;
    };
}
$src = get_template_directory_uri();
?>

<section class="section faq_full_width" id="<?= $id; ?>">
    <div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>">
        <div class="faq_full_width__content">
            <div class="faq_full_width__column ">
                <?php  foreach($title_array as $key => $ii){
                        printf(
                            "<p class='faq_full_width__column__title ' data-category='%s' >%s</p>",
                            $ii->id, 
                            $ii->title
                        );
                    }
                    ?>
            </div>
            <div class="faq_full_width__small_content">
                <?php  foreach($content_array as $key => $kk){
            
                    
                    $class_active = ($kk->id != 0)? 'unactive':'';
                        printf(
                            "<div class='faq_full_width__small_content__box %s' data-category='%s' data-index='%s'>
                                <p class='faq_full_width__small_content__title'>%s</p>
                                <div class='faq_full_width__small_content__descbox'><p class='faq_full_width__small_content__desc'>%s</p><div class='cute-1'></div></div>
                            </div>",
                            $class_active ,
                            $kk->id, 
                            $kk->index,
                            $kk->title, 
                            $kk->desc
                        );
                    };
                    ?>
            </div>
        </div>
    </div>
</section>
<script type='text/javascript'>
    <?php
    $title_array_js = json_encode($title_array);
    $content_array_js = json_encode($content_array);
    echo "var title_array_js = ". $title_array_js . ";\n; var content_array_js = ". $content_array_js . ";\n";
    ?>
</script>
