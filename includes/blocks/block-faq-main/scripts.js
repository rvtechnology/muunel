import { gsap } from "gsap";
import { ScrollToPlugin } from "gsap/ScrollToPlugin";

gsap.registerPlugin(ScrollToPlugin);

export const sectionFaqFullwidth = () => {    
    jQuery(document).ready(function ($) {
     
        let cateogoryTabsInFaq = document.querySelectorAll('.faq_full_width__column__title');
        let contentBoxsInFaq = document.querySelectorAll('.faq_full_width__small_content__box');
        let contentBoxsDescInFaq = document.querySelectorAll('.faq_full_width__small_content__descbox');
        triggerClickOnCategoryInFaq()
        let footerElementFaq = document.querySelectorAll('.footer .column-3 li a');
        footerElementFaq.forEach((ele)=>{
          
            ele.addEventListener('click', function () {
                //rozwiazuje problem z podwójnym klikaniem w link w stopce, funkcja zdazy zadzialac zanim link zmieni url
                setTimeout(triggerClickOnCategoryInFaq, 250)
            });
        })
        window.addEventListener('hashchange', triggerClickOnCategoryInFaq, false);
        function triggerClickOnCategoryInFaq(){
           console.log(location.hash)
            let currentUrlPage = location.href;
            let parmsFromUrl = currentUrlPage.split('#')[1];
            
            if(parmsFromUrl){
                // console.log('t');
                gsap.to(window, { duration: 2, scrollTo: {y: ".faq_full_width", offsetY: 50} });

                let setFirstElementActive = true;
                cateogoryTabsInFaq.forEach((element) => {
                    if(element.getAttribute('data-category') == parmsFromUrl ){
                        element.classList.add('active'); 
                    }else{
                        element.classList.remove('active'); 
                    };
                });
                contentBoxsInFaq.forEach((element) => {
                    if(element.getAttribute('data-category') == parmsFromUrl ){
                        element.classList.remove('unactive'); 
                        // console.log('active fields')
                        if(setFirstElementActive){   
                            element.classList.add('active');

                            let children = element.querySelectorAll('.faq_full_width__small_content__descbox')[0];
                            children.style.display = "block";
                            setFirstElementActive = false;
                        }

                    }else{
                        element.classList.add('unactive'); 
                    };     
                });
            }
        }

        $('.faq_full_width .faq_full_width__column__title').on('click',function(){
            $('.faq_full_width .faq_full_width__column__title').removeClass('active');
            $(this).addClass('active');
            let currentCategory = $(this).attr('data-category');
            location.hash = `#${currentCategory}`;
            $('.faq_full_width__small_content__box').addClass('unactive').removeClass('active');
            $('.faq_full_width__small_content__box[data-category=' + currentCategory + ']').removeClass('unactive');

            if(window.innerWidth < 991) {
                gsap.to(window, { duration: 1, scrollTo: {y: ".faq_full_width__small_content", offsetY: 100} });
            }
        })
        $('.faq_full_width .faq_full_width__small_content__title').on('click',function(){
            let parent = $(this).parent();
            if(parent.hasClass('active')){
            parent.removeClass('active').children('.faq_full_width__small_content__descbox').slideUp();
            
            }else{
            $('.faq_full_width__small_content__descbox').slideUp();
            $('.faq_full_width .faq_full_width__small_content__box').removeClass('active');
            parent.addClass('active').children('.faq_full_width__small_content__descbox').slideDown();
            }
        });
        $('.faq_full_width__search_box img').on('click',function(){
            sortFaqContent()
        });
        $('.faq_full_width__search_box input').keyup(function(event) {
            if (event.keyCode === 13) {
                sortFaqContent();
            }
        });
        
        function sortFaqContent(){
            let searchFaqInputValue = document.querySelector('.faq_full_width__search_box input').value.toLowerCase();
            if(searchFaqInputValue.replace(/\s/g, "") != ''){
                let setFirstElementActive = true;
                
                cateogoryTabsInFaq.forEach((element) => {
                        element.classList.remove('active');   
                });
                contentBoxsInFaq.forEach((element) => {
                    element.classList.remove('active');
                    element.classList.add('unactive');     
                });
                contentBoxsDescInFaq.forEach((element) => {
                    element.style.display = "none";  
                });
                content_array_js.forEach(function(value,index){
                    if(value.title.toLowerCase().indexOf(searchFaqInputValue) > -1){                    
                        cateogoryTabsInFaq.forEach((element) => {
                            if(element.getAttribute('data-category') == value.id ){
                                element.classList.add('active');
                            }
                        });
                    }
                    if(value.desc.toLowerCase().indexOf(searchFaqInputValue) > -1){
                        // cateogoryTabsInFaq.forEach((element) => {
                        //     if(element.getAttribute('data-category') == value.id ){
                        //         element.classList.add('active');
                        //     }
                        // });

                        if(window.innerWidth < 991) {
                            gsap.to(window, { duration: 1, scrollTo: {y: ".faq_full_width__small_content", offsetY: 100} });
                        }
                       
                        contentBoxsInFaq.forEach((element) => {
                            if(element.getAttribute('data-index') == value.index ){
                                element.classList.remove('unactive');
                                // element.classList.add('active');
                                if(setFirstElementActive){   
                                    element.classList.add('active');
 
                                    let children = element.querySelectorAll('.faq_full_width__small_content__descbox')[0];
                                    children.style.display = "block";
                                    setFirstElementActive = false;
                                }
                            }
                        });
                    }
                })
            }
           
        }
    });
}