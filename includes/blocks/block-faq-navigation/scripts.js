const faqBlocks = document.querySelectorAll('.questionAnswer');
const faqNavigationWrapper = document.querySelector('.faqNavigation__content');

export const initFaqNavigation = () => {
    if(faqBlocks && faqNavigationWrapper){
        faqNavigationWrapper.innerHTML = "";

        faqBlocks.forEach(item => {
            let link = document.createElement('div');
            link.innerHTML = item.innerText;
            link.classList.add('paragraph-4');
            link.classList.add('faqNavigation__content--item');
            link.onclick = () => {
                item.querySelector('.faq-single-box').click();
                let posY = item.offsetTop - 110;
                window.scrollTo({
                    top: posY,
                    behavior: 'smooth'
                });
            }
            faqNavigationWrapper.appendChild(link);
        })
    }
}
