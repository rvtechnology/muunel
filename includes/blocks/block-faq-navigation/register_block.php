<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-faq-navigation',
            'title'				=> __('FAQ Navigation'),
            'description'		=> __('FAQ Navigation'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'FAQ Navigation', 'FAQ', 'Navigation', 'blueowl' ),
            'mode'				=> 'edit',
        )
    );
}