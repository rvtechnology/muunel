<?php
$id = 'blockReadingTest-' . $block['id'];
?>
<section class="section blockReadingTest <?php echo get_field('style_changer');?>" id="<?= $id; ?>">
    <div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>">
        <div class="blockReadingTest__sizes">
        <?php
        $distances = get_field('distances');
        $fontsize = 8;
        if (wp_is_mobile()) {
            $fontsize = 4;
        }
        foreach ($distances as $single ) {
        ?>
            <div class="single-distance">
                <div class="number"><?php echo $single['number'];?></div>
                <div class="description" style="font-size: <?php echo ++$count*$fontsize;?>px"><?php echo $single['desc'];?></div>
            </div>
        <?php
        }
        ?>
        </div>
    </div>
</section>