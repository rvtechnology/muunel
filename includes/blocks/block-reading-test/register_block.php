<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-reading-test',
            'title'				=> __('Reading Test'),
            'description'		=> __('Reading Test'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Reading Test', 'blueowl' ),  
            'mode'				=> 'edit',
        )
    );
}