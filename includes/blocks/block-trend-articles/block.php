<?php
$id = 'trendArticles-' . $block['id'];
?>
<section class="section trendArticles" id="<?= $id; ?>">
    <div class="<?=$block['className']?><?=$block['align'] ? ' align'.$block['align'] : '';?>">
    <?php 
    wpb_set_post_views(get_the_ID());
    wpb_get_post_views(get_the_ID());
    $popularpost = new WP_Query( array( 'posts_per_page' => 6, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
?>
        <h3 class="splide__slide__content"><?= __("Trending Articles", "muunel"); ?></h3>
        <div class="splide trendArticles__sliders" >
            <div class="splide__track">
                <ul class="splide__list">
                <?php while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>
                <?php global $post; ?>
                    <li class="splide__slide" style="background-image: url('<?php the_post_thumbnail_url( );?>')">
                        <h4><?php the_title(); ?></h4>
                        <p><?php the_excerpt(); ?></p>
                        <div class="row">
                            <p class="date"><?=get_the_date('d M Y', $post);?></p>
                            <a href="<?php the_permalink( );?>" class="read-more"><?= __("Read more", "muunel"); ?></a>
                        </div>
                    </li>
                <?php endwhile; ?>
                </ul>
            </div>

        </div>
    </div>
</section>

	


