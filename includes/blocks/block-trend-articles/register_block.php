<?php
if( function_exists('acf_register_block_type') ) {
    acf_register_block_type(	
        array(
            'name'				=> 'block-trend-articles',
            'title'				=> __('Trend Articles'),
            'description'		=> __('Trend Articles'),
            'render_template'   => dirname(__FILE__) . '/block.php',
            'category'			=> 'blueowl_block_categories',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'Trend Articles', 'blueowl' ),
            'mode'				=> 'edit',
        )
    );
}