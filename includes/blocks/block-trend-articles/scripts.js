import Splide from '@splidejs/splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
var SimpleLightbox = require('simple-lightbox');
import 'simple-lightbox/dist/simpleLightbox.css';

export function trendArticlesScripts() {
    document.addEventListener( 'DOMContentLoaded', function () {
        const blocks = document.querySelectorAll('.trendArticles__sliders');
        if (blocks) {
            blocks.forEach(block=>{
                new Splide(block, {
                    cover: true,
                    arrows: false,
                    type: "loop",
                    perPage: 2,
                    perMove: 1,
                    gap: "20px",
                    breakpoints: {
                        1110: {
                            perPage: 2,
                        },
                        768: {
                            perPage: 1,
                            fixedWidth: '500px'
                        },
                        480: {
                            perPage: 1,
                            fixedWidth: '300px'
                        },
                    }
                }).mount();
            })
        }
    })
}