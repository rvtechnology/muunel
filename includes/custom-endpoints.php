<?php
add_action('wp_ajax_nopriv_get_variation_gallery', 'get_variation_gallery');
add_action('wp_ajax_get_variation_gallery', 'get_variation_gallery');
function get_variation_gallery(){
    $id = find_matching_product_variation_id($_REQUEST['id'], [
        'attribute_pa_color' => $_REQUEST['color'],
        'attribute_pa_prescriptions' => 'non-prescription',
    ]);
    $gallery = get_field('variation_gallery', (int)$id);
    $result = array(
        'gallery' => null,
        'variation_price' => wc_price(wc_get_product($id)->get_price()),
        'modal' => null
    );
    if($gallery){
        $gallery_arr = array();
        $iter = 0;
        foreach($gallery as $image){
            $img_vid = array(
                'image' => null,
                'is_video' => false,
                'video' => null
            );
            $img_vid['image'] = '<li class="splide__slide" data-id="'.$iter.'"><img class="splide__slide--item" src="'.wp_get_attachment_image_src($image, 'productImage')[0].'" alt="gallery-item-'.$iter.'"></li>';

            if(get_field('is_video', $image) == true){
                $img_vid['is_video'] = true;
                $img_vid['image'] = '<li class="splide__slide video_slide" data-id="'.$iter.'"><img class="splide__slide--item" src="'.wp_get_attachment_image_src($image, 'productImage')[0].'" alt="gallery-item-'.$iter.'"></li>';
                $img_vid['video'] = get_field("video_url", $image);
            }
            array_push($gallery_arr, $img_vid);
            $iter++;
        }
        $result['gallery'] = $gallery_arr;
    }
    $imageModal = get_field('img_modal', (int)$id);
    $imageModal = $imageModal['sizes']['fullHD'];
    $anotherData = array(
        'img_modal' => $imageModal,
        'frame_size' => get_field('frame_size', (int)$id),
        'standard_measurements' => get_field('standard_measurements', (int)$id),
        'total_frame_width' => get_field('total_frame_width', (int)$id),
    );

    $result['modal'] = $anotherData;

    echo json_encode($result);
    wp_die();
}

add_action('wp_ajax_nopriv_remove_coupon_minicart', 'remove_coupon_minicart');
add_action('wp_ajax_remove_coupon_minicart', 'remove_coupon_minicart');
function remove_coupon_minicart(){
    echo json_encode(WC()->cart->remove_coupons());
    wp_die();
}

add_action('wp_ajax_nopriv_add_coupon_minicart', 'add_coupon_minicart');
add_action('wp_ajax_add_coupon_minicart', 'add_coupon_minicart');
function add_coupon_minicart(){
    $coupon = $_REQUEST['code'];
    $result = array();
    if($coupon){
        WC()->cart->remove_coupons();
        $result['code'] = WC()->cart->add_discount( $coupon );
    }
    if($result['code'] == false){
        $result['error'] = __('The code is not valid', 'muunel');
        $result['code'] = false;
    }
    echo json_encode($result);
    wp_die();
}

add_action('wp_ajax_nopriv_product_add_to_cart', 'product_add_to_cart');
add_action('wp_ajax_product_add_to_cart', 'product_add_to_cart');
function product_add_to_cart(){
    global $woocommerce;
    if(isset($_REQUEST['variation_id']) && $_REQUEST['variation_id'] != 0){
        $product_id = (int)$_REQUEST['product_id'];
        $variation_id = (int)$_REQUEST['variation_id'];
        $quantity = (int)$_REQUEST['quantity'];
        $prescription_data = array();
        $cart_item_data = array(
            'custom_data' => array(
                'additional_price' => $_REQUEST['additional_price'],
                'additional_name' => $_REQUEST['additional_name']
            )
        );
        if($_REQUEST['pa_prescriptions'] == "prescription"){
            if($_REQUEST['prescription_upload'] == "false"){
                $prescription_data = array(
                    'right_eye_sphere' => $_REQUEST['prescription_data'][0],
                    'right_eye_cylinder' => $_REQUEST['prescription_data'][1],
                    'right_axis' => $_REQUEST['prescription_data'][2],
                    'left_eye_sphere' => $_REQUEST['prescription_data'][3],
                    'left_eye_cylinder' => $_REQUEST['prescription_data'][4],
                    'left_axis' => $_REQUEST['prescription_data'][5]
                );
                if($_REQUEST['prescription_data'][6] != "null"){
                    $prescription_data['single_pd'] = $_REQUEST['prescription_data'][6];
                } else if($_REQUEST['prescription_data'][7] != "null" && $_REQUEST['prescription_data'][8] != "null"){
                    $prescription_data['left_pd'] = $_REQUEST['prescription_data'][7];
                    $prescription_data['right_pd'] = $_REQUEST['prescription_data'][8];
                }
                $cart_item_data['custom_data']['prescription_data'] = $prescription_data;
            } else {
                $cart_item_data['custom_data']['prescription_upload'] = true;
                $cart_item_data['custom_data']['uploaded_files'] = array();
                for($i = 0; $i < sizeof($_REQUEST['prescription_upload_files']); $i+=2){
                    $file = array(
                        'title' => $_REQUEST['prescription_upload_files'][$i],
                        'url' => $_REQUEST['prescription_upload_files'][$i+1]
                    );
                    array_push($cart_item_data['custom_data']['uploaded_files'], $file);
                }
            }
        }
        $var = wc_get_product($_REQUEST['variation_id']);
        $result = array(
            'code' => false,
            'cart' => null,
            'count' => null
        );
        if($var->stock_quantity > 0){
            WC()->cart->add_to_cart($product_id, $quantity, $variation_id, null, $cart_item_data);
            WC()->cart->calculate_totals();
            WC()->cart->set_session();
            WC()->cart->maybe_set_cart_cookies();
            $result['code'] = true;
            $result['cart'] = get_mini_cart();
            $result['count'] = WC()->cart->cart_contents_count;
            echo json_encode($result);
        } else {
            echo json_encode($result);
        }
        // echo $variation;
    }
    wp_die();
}

add_action('wp_ajax_nopriv_get_all_posts', 'get_all_posts');
add_action('wp_ajax_get_all_posts', 'get_all_posts');
function get_all_posts(){
    $per_page = $_REQUEST['per_page'];
    $paged = $_REQUEST['page'];
    $category = $_REQUEST['categories'];
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => $per_page,
        'paged' => $paged,
        'category_name' => $category,
        'suppress_filters' => false,
    );
    $query = new WP_Query($args);
    $result = array(
        'max_num_pages' => $query->max_num_pages,
        'error' => null,
        'posts' => null
    );
    $posts = array();
    if($query->have_posts()){
        while($query->have_posts()){
            $query->the_post();
            array_push($posts, make_posts_card(get_the_ID()));
        }
    } else {
        $result['error'] = '<h2 class="no_posts_found">'.__('There is no more posts!', 'muunel').'</h2>';
    }
    $result['posts'] = $posts;
    
    echo json_encode($result);
    wp_die();
}
function make_posts_card($post_id){
    $post = get_post($post_id);
    ob_start();
    ?>
    <div id="postCard-<?=$post->ID?>" class="blogLoop__wrapper--item">
        
        <a href="<?=get_permalink($post)?>">
            <div class="image" style="background-image: url(<?=get_the_post_thumbnail_url($post, 'productImage');?>)"></div>
        </a>
        <p class="paragraph-2"><?=wp_trim_words($post->post_title, 8, '...');?></p>
        <p class="paragraph-5"><?=wp_trim_words($post->post_excerpt, 15, '...');?></p>
        <div class="blogLoop__wrapper--item__meta">
            <div class="date"><?=get_the_date('d M Y', $post);?></div>
            <a href="<?=get_permalink($post)?>"><?=__('Read More', 'muunel');?></a>
        </div>
    </div>
    <?php
    return ob_get_clean();
}
add_action('wp_ajax_nopriv_get_variation_image', 'get_variation_image');
add_action('wp_ajax_get_variation_image', 'get_variation_image');
function get_variation_image(){
    $id = find_matching_product_variation_id($_REQUEST['id'], [
        'attribute_pa_color' => $_REQUEST['color'],
        'attribute_pa_prescriptions' => 'non-prescription',
    ]);

    echo json_encode(wp_get_attachment_image_src( wc_get_product((int)$id)->image_id), 'productImage');
    wp_die();
}
add_action('wp_ajax_nopriv_upload_prescription_file', 'upload_prescription_file');
add_action('wp_ajax_upload_prescription_file', 'upload_prescription_file');
function upload_prescription_file(){
    $result = array(
        'code' => false,
        'file' => null,
        'error' => '',
        'html' => ''
    );
    if($_FILES['file']){
        $allowed_type = array('image/jpeg', 'image/png', 'application/pdf');
        if(in_array($_FILES['file']['type'], $allowed_type)){
            add_filter( 'upload_dir', 'prescriptions_upload_dir' );
                $filename = uniqid().'_'.sanitize_file_name($_FILES['file']['name']);
                $upload = wp_upload_bits($filename, null, file_get_contents($_FILES["file"]["tmp_name"]), '');
                if(!$upload['error']){
                    $result['code'] = true;
                    ob_start(); ?>
                    <div class="file-item">
                        <svg width="38" height="48" viewBox="0 0 38 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="38" height="48" fill="white"/>
                            <path d="M26 0H0V48H38V12H26V0Z" fill="#CCD8CD"/>
                            <path d="M26 0L38 12H26V0Z" fill="#99B09B"/>
                            <?php if($_FILES['file']['type'] == 'image/pdf'): ?>
                            <path d="M10.0081 21.6C10.7681 21.6 11.4281 21.728 11.9881 21.984C12.5481 22.232 12.9801 22.592 13.2841 23.064C13.5881 23.528 13.7401 24.076 13.7401 24.708C13.7401 25.34 13.5881 25.888 13.2841 26.352C12.9801 26.816 12.5481 27.176 11.9881 27.432C11.4281 27.68 10.7681 27.804 10.0081 27.804H8.54413V30H6.16813V21.6H10.0081ZM9.86413 25.932C10.3521 25.932 10.7201 25.828 10.9681 25.62C11.2161 25.404 11.3401 25.1 11.3401 24.708C11.3401 24.316 11.2161 24.012 10.9681 23.796C10.7201 23.58 10.3521 23.472 9.86413 23.472H8.54413V25.932H9.86413ZM14.8986 21.6H18.8706C19.7906 21.6 20.6066 21.772 21.3186 22.116C22.0306 22.46 22.5826 22.948 22.9746 23.58C23.3666 24.212 23.5626 24.952 23.5626 25.8C23.5626 26.648 23.3666 27.388 22.9746 28.02C22.5826 28.652 22.0306 29.14 21.3186 29.484C20.6066 29.828 19.7906 30 18.8706 30H14.8986V21.6ZM18.7746 28.104C19.4946 28.104 20.0706 27.9 20.5026 27.492C20.9426 27.084 21.1626 26.52 21.1626 25.8C21.1626 25.08 20.9426 24.516 20.5026 24.108C20.0706 23.7 19.4946 23.496 18.7746 23.496H17.2746V28.104H18.7746ZM27.1887 23.436V25.284H30.8967V27.12H27.1887V30H24.8127V21.6H31.4007V23.436H27.1887Z" fill="#546756"/>
                            <?php elseif($_FILES['file']['type'] == 'image/jpeg'): ?>
                            <path d="M4.82834 30.168C4.21234 30.168 3.65234 30.064 3.14834 29.856C2.64434 29.64 2.23234 29.332 1.91234 28.932L3.20834 27.396C3.65634 27.98 4.14034 28.272 4.66034 28.272C5.00434 28.272 5.26434 28.168 5.44034 27.96C5.62434 27.752 5.71634 27.448 5.71634 27.048V23.436H2.81234V21.6H8.06834V26.904C8.06834 27.992 7.79234 28.808 7.24034 29.352C6.69634 29.896 5.89234 30.168 4.82834 30.168ZM13.512 21.6C14.272 21.6 14.932 21.728 15.492 21.984C16.052 22.232 16.484 22.592 16.788 23.064C17.092 23.528 17.244 24.076 17.244 24.708C17.244 25.34 17.092 25.888 16.788 26.352C16.484 26.816 16.052 27.176 15.492 27.432C14.932 27.68 14.272 27.804 13.512 27.804H12.048V30H9.67203V21.6H13.512ZM13.368 25.932C13.856 25.932 14.224 25.828 14.472 25.62C14.72 25.404 14.844 25.1 14.844 24.708C14.844 24.316 14.72 24.012 14.472 23.796C14.224 23.58 13.856 23.472 13.368 23.472H12.048V25.932H13.368ZM25.1465 28.164V30H18.4025V21.6H24.9905V23.436H20.7545V24.852H24.4865V26.628H20.7545V28.164H25.1465ZM32.0813 25.62H34.1813V29.1C33.7013 29.444 33.1493 29.708 32.5253 29.892C31.9013 30.076 31.2773 30.168 30.6533 30.168C29.7733 30.168 28.9813 29.984 28.2773 29.616C27.5733 29.24 27.0213 28.72 26.6213 28.056C26.2213 27.392 26.0213 26.64 26.0213 25.8C26.0213 24.96 26.2213 24.208 26.6213 23.544C27.0213 22.88 27.5773 22.364 28.2893 21.996C29.0013 21.62 29.8053 21.432 30.7013 21.432C31.4853 21.432 32.1893 21.564 32.8133 21.828C33.4373 22.092 33.9573 22.472 34.3733 22.968L32.8613 24.336C32.2933 23.712 31.6133 23.4 30.8213 23.4C30.1013 23.4 29.5213 23.62 29.0813 24.06C28.6413 24.492 28.4213 25.072 28.4213 25.8C28.4213 26.264 28.5213 26.68 28.7213 27.048C28.9213 27.408 29.2013 27.692 29.5613 27.9C29.9213 28.1 30.3333 28.2 30.7973 28.2C31.2533 28.2 31.6813 28.108 32.0813 27.924V25.62Z" fill="#546756"/>
                            <?php else: ?>
                            <path d="M8.86359 21.6C9.62359 21.6 10.2836 21.728 10.8436 21.984C11.4036 22.232 11.8356 22.592 12.1396 23.064C12.4436 23.528 12.5956 24.076 12.5956 24.708C12.5956 25.34 12.4436 25.888 12.1396 26.352C11.8356 26.816 11.4036 27.176 10.8436 27.432C10.2836 27.68 9.62359 27.804 8.86359 27.804H7.39959V30H5.02359V21.6H8.86359ZM8.71959 25.932C9.20759 25.932 9.57559 25.828 9.82359 25.62C10.0716 25.404 10.1956 25.1 10.1956 24.708C10.1956 24.316 10.0716 24.012 9.82359 23.796C9.57559 23.58 9.20759 23.472 8.71959 23.472H7.39959V25.932H8.71959ZM21.7461 21.6V30H19.7901L16.0821 25.524V30H13.7541V21.6H15.7101L19.4181 26.076V21.6H21.7461ZM29.05 25.62H31.15V29.1C30.67 29.444 30.118 29.708 29.494 29.892C28.87 30.076 28.246 30.168 27.622 30.168C26.742 30.168 25.95 29.984 25.246 29.616C24.542 29.24 23.99 28.72 23.59 28.056C23.19 27.392 22.99 26.64 22.99 25.8C22.99 24.96 23.19 24.208 23.59 23.544C23.99 22.88 24.546 22.364 25.258 21.996C25.97 21.62 26.774 21.432 27.67 21.432C28.454 21.432 29.158 21.564 29.782 21.828C30.406 22.092 30.926 22.472 31.342 22.968L29.83 24.336C29.262 23.712 28.582 23.4 27.79 23.4C27.07 23.4 26.49 23.62 26.05 24.06C25.61 24.492 25.39 25.072 25.39 25.8C25.39 26.264 25.49 26.68 25.69 27.048C25.89 27.408 26.17 27.692 26.53 27.9C26.89 28.1 27.302 28.2 27.766 28.2C28.222 28.2 28.65 28.108 29.05 27.924V25.62Z" fill="#546756"/>
                            <?php endif; ?>
                        </svg>
                        <p class="paragraph-5"><?=$_FILES['file']['name']?></p>
                    </div>
                    <?php
                    $result['html'] = ob_get_clean();
                    $result['file'] = array(
                        'title' => $_FILES['file']['name'],
                        'url' => $upload['url']
                    );
                } else{
                    $result['error'] = __('Unable to upload file: ', 'muunel').$_FILES['file']['name'];
                }
            remove_filter( 'upload_dir', 'prescriptions_upload_dir' );
        } else{
            $result['error'] = __('Not allowed file type: ', 'muunel').$_FILES['file']['name'];
        }
    }
    echo json_encode($result);
    wp_die();
}
function prescriptions_upload_dir( $dirs ) {
    $dirs['subdir'] = '/muunel-prescriptions';
    $dirs['path'] = $dirs['basedir'] . '/muunel-prescriptions';
    $dirs['url'] = $dirs['baseurl'] . '/muunel-prescriptions';
    if(!file_exists($dirs['path'])){
        wp_mkdir_p($dirs['path']);
    }
    return $dirs;
}

add_action('wp_ajax_nopriv_is_variation_on_stock', 'is_variation_on_stock');
add_action('wp_ajax_is_variation_on_stock', 'is_variation_on_stock');
function is_variation_on_stock(){
    $id = find_matching_product_variation_id($_REQUEST['id'], [
        'attribute_pa_color' => $_REQUEST['color'],
        'attribute_pa_prescriptions' => $_REQUEST['prescription'],
    ]);
    $product = wc_get_product($id);
    if($product->managing_stock() && $product->is_in_stock()){
        echo json_encode(true);
    } else{
        echo json_encode(false);
    }
    wp_die();
}