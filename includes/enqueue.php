<?php

// Load scripts
function load_css_js() {
	wp_enqueue_style('main-css', get_template_directory_uri() . '/dist/style.css', null, '1.1');
    wp_enqueue_script('main-scripts', get_template_directory_uri() . '/dist/script.js', array('jquery'), null, true);
    wp_enqueue_script('awesome-scripts', 'https://kit.fontawesome.com/271b6bfbd1.js', array('jquery'), null, false);

    wp_localize_script( 'main-scripts', 'ajax', array(
        'url' => admin_url( 'admin-ajax.php' ),
        'nonce' => wp_create_nonce('ajaxnonce')
    ) );
}
add_action('wp_enqueue_scripts', 'load_css_js', 100);