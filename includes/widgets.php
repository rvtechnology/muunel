<?php
/**********
	Add your custom sidebar here 
**********/

register_sidebar( array(
	'name' => 'Shop Widget',
	'id' => 'shop-widget',
	'description' => 'Shop widgets',
	'before_title'  => '<p class="widgettitle">',
	'after_title'   => '</p>',
) );

register_sidebar( array(
  'name' => 'Shop Filter',
  'id' => 'shop-filter',
  'description' => 'Shop filter',
  'before_title'  => '<p class="customShopFilterWidgetTitle">',
  'after_title'   => '</p>',
) );

register_sidebar( array(
	'name' => 'Checkout sidebar',
	'id' => 'checkout',
	'description' => 'Checkout sidebar',
	'before_title'  => '<p class="checkout-sidebar__title">',
	'after_title'   => '</p>',
) );


register_sidebar( array(
	'name' => 'Footer widget',
	'id' => 'footer-widget',
	'description' => 'Footer widget',
	'before_title'  => '<p class="footer-widget__title">',
	'after_title'   => '</p>',
) );