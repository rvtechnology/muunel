<?php

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
        'page_title'    =>  'Muunel Settings',
        'menu_title'    =>  'Muunel Settings',
        'menu_slug'     =>  'main-settings',
        'capability'    =>  'edit_posts',
        'parent_slug'   =>  '',
        'position'      =>  false,
        'icon_url'      =>  false,
    ));

    acf_add_options_sub_page(array(
        'page_title'    =>  'General Settings',
        'menu_title'    =>  'General Settings',
        'menu_slug'     =>  'site-settings',
        'capability'    =>  'edit_posts',
        'parent_slug'   =>  'main-settings',
        'position'      =>  false,
        'icon_url'      =>  false,
        // 'post_id'       => 'site-settings',
        'redirect'      => false
    ));

    // acf_add_options_sub_page(array(
    //     'page_title'    =>  'Contact Settings',
    //     'menu_title'    =>  'Contact Settings',
    //     'menu_slug'     =>  'contact-settings',
    //     'capability'    =>  'edit_posts',
    //     'parent_slug'   =>  'main-settings',
    //     'position'      =>  false,
    //     'icon_url'      =>  false,
    //     // 'post_id'       => 'contact-settings'
    // ));

    acf_add_options_sub_page(array(
        'page_title'    =>  'Lenses Settings',
        'menu_title'    =>  'Lenses Settings',
        'menu_slug'     =>  'Lenses-settings',
        'capability'    =>  'edit_posts',
        'parent_slug'   =>  'main-settings',
        'position'      =>  false,
        'icon_url'      =>  false,
        // 'post_id'       => 'footer-settings'
    ));
    
    acf_add_options_sub_page(array(
        'page_title'    =>  'Footer Settings',
        'menu_title'    =>  'Footer Settings',
        'menu_slug'     =>  'footer-settings',
        'capability'    =>  'edit_posts',
        'parent_slug'   =>  'main-settings',
        'position'      =>  false,
        'icon_url'      =>  false,
        // 'post_id'       => 'footer-settings'
    ));
}


