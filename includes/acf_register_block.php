<?php 

// add custom category to gutenberg blocks
function bo_block_category( $categories, $post ) {
	if ( $post->post_type == 'page' || $post->post_type == 'post' || $post->post_type == 'product' ) { 
		$welcoop_blocks = array_merge(
			$categories,
			array(
				array(
					'slug' => 'blueowl_block_categories',
					'title' => __( 'Blue Owl Blocks', 'muunel' ),
				)
			)
		);
	} else {
		return $categories;
	}
	return $welcoop_blocks;
}
add_filter( 'block_categories', 'bo_block_category', 10, 2);

// enable gutenberg edit page for products
function bo_activate_gutenberg_products($can_edit, $post_type){
	if($post_type == 'product'){
		$can_edit = true;
	}
	
	return $can_edit;
}
add_filter('use_block_editor_for_post_type', 'bo_activate_gutenberg_products', 10, 2);

//enable category in gutenberg edit page
function bo_activate_gutenberg_products_cat( $args, $taxonomy_name ) {
	if ( 'product_tag' === $taxonomy_name || 'product_cat' === $taxonomy_name ) {
		$args['show_in_rest'] = true;
	}
	
	return $args;
}
add_filter( 'register_taxonomy_args', 'bo_activate_gutenberg_products_cat', 10, 2 );

// define custom gutenberg blocks
add_action('acf/init', 'blueowl_acf_blocks_init');
function blueowl_acf_blocks_init() {
	require get_template_directory() .'/includes/blocks/block-bullets/register_block.php';
	require get_template_directory() .'/includes/blocks/block-main-carousel/register_block.php';
	require get_template_directory() .'/includes/blocks/block-reading-test/register_block.php';
	 require get_template_directory() .'/includes/blocks/block-product-carousel/register_block.php';
	require get_template_directory() .'/includes/blocks/block-banner-single-site/register_block.php';
	require get_template_directory() .'/includes/blocks/block-banner-relative/register_block.php';
	require get_template_directory() .'/includes/blocks/block-banner-getstarted/register_block.php';
	require get_template_directory() .'/includes/blocks/block-banner-collection/register_block.php';
	require get_template_directory() .'/includes/blocks/block-product-reviews/register_block.php';
	require get_template_directory() .'/includes/blocks/block-promoted-posts/register_block.php';
	require get_template_directory() .'/includes/blocks/block-benefit-lists/register_block.php';
	require get_template_directory() .'/includes/blocks/block-banner-homepage/register_block.php';
	require get_template_directory() .'/includes/blocks/block-banner-homepage-people/register_block.php';
	require get_template_directory() .'/includes/blocks/block-switch-page/register_block.php';
	require get_template_directory() .'/includes/blocks/block-policy-and-terms/register_block.php';
	require get_template_directory() .'/includes/blocks/block-letter-separator/register_block.php';
	require get_template_directory() .'/includes/blocks/block-letter-navigation/register_block.php';
	require get_template_directory() .'/includes/blocks/block-question-answer/register_block.php';
	require get_template_directory() .'/includes/blocks/block-contact/register_block.php';
	require get_template_directory() .'/includes/blocks/block-faq-main/register_block.php';
	require get_template_directory() .'/includes/blocks/block-product-bullets/register_block.php';
	require get_template_directory() .'/includes/blocks/block-single-image/register_block.php';
	// require get_template_directory() .'/includes/blocks/block-reviews/register_block.php';
}
