<?php
/**
 * Wocommerce settings
 */
add_action( 'after_setup_theme', 'theme_woocommerce_support', 99 );
function theme_woocommerce_support() {
  add_theme_support( 'woocommerce' );
}

// custom wrapper
add_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper');
// overwrite existing output content wrapper function
function woocommerce_output_content_wrapper() {
  ?>
	<div class="pageWrapper">
    <div class="page-woocommerce">
  <?php
}
add_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end');
function woocommerce_output_content_wrapper_end() {
  ?>
		  </div><!-- Close Column -->
		</div><!-- Close Row -->
  <?php
}

// custom fields in register form
add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );
function wooc_extra_register_fields() {
  woocommerce_form_field(
    'billing_first_name',
    array(
      'type'        => 'text',
      'required'    => true, // just adds an "*"
      'label'       => __('First name', 'muunel')
    ),
    ( isset($_POST['billing_first_name']) ? $_POST['billing_first_name'] : '' )
  );
  
  woocommerce_form_field(
    'billing_last_name',
    array(
      'type'        => 'text',
      'required'    => true, // just adds an "*"
      'label'       => __('Last name', 'muunel')
    ),
    ( isset($_POST['billing_last_name']) ? $_POST['billing_last_name'] : '' )
  );
}

/**
* register fields Validating.
*/
add_action( 'woocommerce_register_post', 'misha_validate_fields', 10, 3 );
function misha_validate_fields( $username, $email, $errors ) {
	if ( empty( $_POST['billing_first_name'] ) ) {
		$errors->add( 'billing_first_name_error', __('Please enter first name field', 'muunel') );
  }
  
  if ( empty( $_POST['billing_last_name'] ) ) {
		$errors->add( 'billing_last_name', __('Please enter last name field', 'muunel') );
	}
}

/**
 * Add the code below to your theme's functions.php file
 * to add a confirm password field on the register form under My Accounts.
 */ 
function woocommerce_registration_errors_validation($reg_errors, $sanitized_user_login, $user_email) {
	global $woocommerce;
	extract( $_POST );
	if ( strcmp( $password, $password2 ) !== 0 ) {
		return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
	}
	return $reg_errors;
}
add_filter('woocommerce_registration_errors', 'woocommerce_registration_errors_validation', 10, 3);

add_filter( 'woocommerce_shipping_fields' , 'edit_shipping_fields' );
function edit_shipping_fields( $fields ) {
  unset($fields['shipping_email']);
  unset($fields['shipping_phone']);

  return $fields;
}

// // Customization checkout fields
add_filter( 'woocommerce_default_address_fields' , 'custom_accountdata_fields', 10 );
function custom_accountdata_fields( $fields ) {
  //removing fields
  unset($fields['company']);
  $fields['city']['label'] = __('City', 'muunel');
  $fields['country']['label'] = __('Country', 'muunel');
  $fields['postcode']['label'] = __('ZIP', 'muunel');
  $fields['address_1']['label'] = __('Address Line 1', 'muunel');
  $fields['address_2']['label'] = __('Address Line 2', 'muunel');
  $fields['address_2']['label_class'] = '';
  $fields['state']['label'] = __('State', 'muunel');

  //reordering fields
  $fields['first_name']['priority'] = 1;
  $fields['last_name']['priority'] = 2;
  $fields['email']['priority'] = 5;
  $fields['phone']['priority'] = 3;
  $fields['address_1']['priority'] = 7;
  $fields['address_2']['priority'] = 8;
  $fields['postcode']['priority'] = 10;
  $fields['city']['priority'] = 64;
  $fields['country']['priority'] = 121;
  $fields['state']['priority'] = 122;

  return $fields;
}

add_filter( 'woocommerce_checkout_fields', 'checkout_fields', 10 );
function checkout_fields( $checkout_fields ) {
  $checkout_fields['billing']['billing_phone']['priority'] = 3;
  $checkout_fields['billing']['billing_email']['priority'] = 5;
  $checkout_fields['billing']['billing_country']['priority'] = 8;
  $checkout_fields['billing']['billing_country']['class'] = array('form-row-last');

  $checkout_fields['billing']['billing_phone']['class'] = array('form-row-last');
  unset($checkout_fields['billing']['billing_company']);

  $checkout_fields['shipping']['shipping_company']['class'] = array('form-row-last');
  $checkout_fields['shipping']['shipping_country']['priority'] = 8;
  $checkout_fields['shipping']['shipping_country']['class'] = array('form-row-first');
  unset($checkout_fields['shipping']['shipping_company']);

	return $checkout_fields;
}

// //remowe downlds from myAccount
function menu_my_account_menu_items( $items ) {
  unset( $items['downloads'] );
  return $items;
}
add_filter( 'woocommerce_account_menu_items', 'menu_my_account_menu_items' );

function my_account_order_menu() {
	$myorder = array(
		'dashboard'          => __('Personal Information', 'muunel'),
		'edit-address'       => __('Shipping Address', 'muunel'),
		'orders'             => __('Purchase History', 'muunel'),
		'edit-account'       => __('Security', 'muunel'),
		'customer-logout'    => __('Logout', 'woocommerce'),
	);

	return $myorder;
}
add_filter ( 'woocommerce_account_menu_items', 'my_account_order_menu' );

//add class to category menu when item has children
function add_category_parent_css($css_classes, $category, $depth, $args){
  if($args['has_children']){
      $css_classes[] = 'has_children';
  }
  return $css_classes;
}

function get_mini_cart(){
    $minicart_acf = get_field('mini_cart', 'options');
    // WC()->cart->calculate_shipping();

    $shipping_cost = WC()->cart->get_shipping_total();
    if($shipping_cost == 0) $shipping_cost = __('Free', 'muunel');
    else $shipping_cost = wc_price($shipping_cost);

    $subtotal = WC()->cart->get_cart_subtotal();
    $discount_total = WC()->cart->get_discount_total();
    $is_coupon = sizeof(WC()->cart->get_applied_coupons());
    // WC()->cart->calculate_totals();
    $total = WC()->cart->get_total();
    ob_start();
    ?>
    <div class="cart-dropdown">
        <div class="cart-dropdown__wrapper">
            <span class="close">
            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M16.5292 1.47059L1.47041 16.5294" stroke="#0B0B0A" stroke-width="1.5"/>
                <path d="M16.5296 16.5294L1.47082 1.47059" stroke="#0B0B0A" stroke-width="1.5"/>
            </svg>
            </span>
            <div class="cart-preview">
                <h3><?=__('Order Summary', 'muunel');?></h3>
                <a href="<?=wc_get_cart_url();?>" disabled><?=__('View Bag', 'muunel');?></a>
                <div class="cart-preview__products">
                    <?php foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) : ?>
                        <?php
                        $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                        $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
                        $parent = wc_get_product($_product->get_parent_id());
                        ?>
                        <?php if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) : ?>
                            <?php $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key); ?>
                            <div class="cart-preview__products--single">
                                <div class="product-thumbnail">
                                    <?php
                                    $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);
                                    if (!$product_permalink) :
                                        echo wp_kses_post($thumbnail);
                                    else :
                                        printf('<a href="%s">%s</a>', esc_url($product_permalink), wp_kses_post($thumbnail));
                                    endif;
                                    ?>
                                </div>
                                <div class="product-name">
                                    <div class="product-name--title"><?=$parent->get_title();?></div>
                                    <div class="product-name--color"><?=$_product->get_attribute('pa_color');?></div>
                                </div>
                                <div class="product-quantity"><?=$cart_item['quantity'];?>x</div>
                                <div class="product-subtotal"><?=wc_price($cart_item['data']->get_price());?></div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
        </div>
            </div>
            <?php if(is_cart()) : do_action( 'woocommerce_before_cart_totals' ); ?>
            <?php else : do_action( 'woocommerce_checkout_before_order_review' ); endif; ?>
            <div class="cart__summary cart_totals">
            <?php if($shipping_cost == 'Free'):?>
              <p class="free_shipping_label"><?=__('You are elligible for free shipping!', 'muunel')?></p>
            <?php endif; ?>
            <div class="cart__summary--item">
              <p><?=__('Subtotal', 'muunel');?>:</p>
              <p class="mini-cart-value"><?=$subtotal;?></p>
            </div>
            <div class="cart__summary--item">
              <p><?=__('Shipping', 'muunel');?>:</p>
              <p class="mini-cart-value"><?=$shipping_cost?></p>
            </div>
            <?php if($is_coupon != 0) { ?>
              <div class="cart__summary--item">
                <p><?=__('Promocode', 'muunel');?>:</p>
                <p class="mini-cart-value">-<?=wc_price($discount_total)?></p>
              </div>
            <?php } ?>
            <div class="cart__summary--price order-total">
              <p><?=__('Total', 'muunel');?>:</p>
              <p class="mini-cart-value-total"><?=$total;?></p>
            </div>
            <form class="cart__summary--coupons">
              <?php
              $coupon_placeholder = __('Coupon Code', 'muunel');
              if($is_coupon != 0){
                $coupon_placeholder = "";
              }
              ?>
              <input type="text" id="coupon-input" data-coupon-error="<?=esc_attr(__('Enter valid coupon!', 'muunel'))?>" placeholder="<?=$coupon_placeholder?>" <?php if($is_coupon != 0){ echo 'disabled'; } ?> value="">
              <?php if($is_coupon != 0): ?>
                <p><?=WC()->cart->applied_coupons[0];?>
                  <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 6.5L4.5 10.5L13.5 1" stroke="black" stroke-width="1.5"/>
                  </svg>
                </p>
                <button id="remove_coupon"><?=__('Remove', 'muunel')?></button>
              <?php else: ?>
                <button id="apply_coupon"><?=__('Apply', 'muunel')?></button>
              <?php endif; ?>
            </form>
            <?php if(is_cart()) : do_action( 'woocommerce_after_cart_totals' );
            else : do_action( 'woocommerce_checkout_after_order_review' ); endif; ?>
            <div class="cart__summary--desc">
                <ul>
                  <?php if($minicart_acf['repeater']): foreach($minicart_acf['repeater'] as $item){
                    ?>
                      <li class="cart-preview__desc--item paragraph-5">
                        <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 6.5L4.5 10.5L13.5 1" stroke="black" stroke-width="1.5"/>
                        </svg>
                        <?=$item['desc']?>
                      </li>
                    <?php
                  } endif; ?>
                </ul>
            </div>
            <a class="button__muunel" href="<?php echo wc_get_checkout_url(); ?>">
              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="2" y="4" width="20" height="16" stroke="white" stroke-width="1.5"/>
                <line x1="2.75" y1="8.25" x2="21.25" y2="8.25" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <line x1="13" y1="15.25" x2="19" y2="15.25" stroke="white" stroke-width="1.5"/>
                <circle cx="6.5" cy="15.5" r="1.5" fill="white"/>
                <circle cx="8.5" cy="15.5" r="1.5" fill="white"/>
              </svg>
              <?=__('Procced To Checkout', 'muunel');?>
            </a>
            </div>
            <?= apply_filters( 'woocommerce_widget_cart_after_price', null) ?>
        </div>
    </div>

  <?php
  return ob_get_clean();
}

function my_hide_shipping_when_free_is_available( $rates ) {
	$free = array();
	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}
	return ! empty( $free ) ? $free : $rates;
}
add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );

function woocommerce_additional_price_to_cart_item( $cart_object ) {
	if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
    return;
  foreach ( $cart_object->get_cart() as $key => $value ) {

      if(!empty($value["custom_data"]["additional_price"])){
          $value['data']->set_price($value['data']->price + $value["custom_data"]["additional_price"]);
      }
  }
}
add_action( 'woocommerce_before_calculate_totals', 'woocommerce_additional_price_to_cart_item', 99 );

/**
 * Add order item meta
 */
add_action('woocommerce_add_order_item_meta', 'add_order_item_meta', 10, 2);
function add_order_item_meta($item_id, $values){
  if (isset($values['custom_data'])) {
    $custom_data  = $values['custom_data'];

    wc_add_order_item_meta($item_id, 'Additional Option', $custom_data['additional_name']);
    
    wc_add_order_item_meta($item_id, 'Left Eye Sphere(SPH)', $custom_data['prescription_data']['left_eye_sphere']);
    wc_add_order_item_meta($item_id, 'Left Eye Cylinder(CYL)', $custom_data['prescription_data']['left_eye_cylinder']);
    wc_add_order_item_meta($item_id, 'Left Eye Axis', $custom_data['prescription_data']['left_axis']);
    wc_add_order_item_meta($item_id, 'Right Eye Sphere(SPH)', $custom_data['prescription_data']['right_eye_sphere']);
    wc_add_order_item_meta($item_id, 'Right Eye Cylinder(CYL)', $custom_data['prescription_data']['right_eye_cylinder']);
    wc_add_order_item_meta($item_id, 'Right Eye Axis', $custom_data['prescription_data']['right_axis']);
    
    if(!empty($custom_data['prescription_data']['single_pd'])){
      wc_add_order_item_meta($item_id, 'Single Eye Pupillary Distance(PD)', $custom_data['prescription_data']['single_pd']);
    } else if(!empty($custom_data['prescription_data']['left_pd']) && !empty($custom_data['prescription_data']['right_pd'])){
      wc_add_order_item_meta($item_id, 'Left Eye Pupillary Distance(PD)', $custom_data['prescription_data']['left_pd']);
      wc_add_order_item_meta($item_id, 'Right Eye Pupillary Distance(PD)', $custom_data['prescription_data']['right_pd']);
    }
    if(!empty($custom_data['prescription_upload'])){
      for($i=0; $i < sizeof($custom_data['uploaded_files']); $i++){
        wc_add_order_item_meta($item_id, 'File_'.$i, '<a target="_blank" href="'.$custom_data['uploaded_files'][$i]['url'].'">'.$custom_data['uploaded_files'][$i]['title'].'</a>');
      }
    }
  }
}
/**
 * Display the custom data on cart and checkout page
 */
add_filter('woocommerce_get_item_data', 'get_item_data', 25, 2);
function get_item_data($other_data, $cart_item){
  if (isset($cart_item['custom_data'])) {
    $custom_data  = $cart_item['custom_data'];

    if (!empty($custom_data['additional_name'])){
      $other_data[] =   array(
          'name' => 'Additional Option',
          'display'  => $custom_data['additional_name']
      );
    }
    if (!empty($custom_data['prescription_data'])){
      $other_data[] =   array(
          'name' => 'Right Eye Sphere(SPH)',
          'display'  => $custom_data['prescription_data']['right_eye_sphere']
      );
      $other_data[] =   array(
          'name' => 'Right Eye Cylinder(CYL)',
          'display'  => $custom_data['prescription_data']['right_eye_cylinder']
      );
      $other_data[] =   array(
          'name' => 'Right Eye Axis',
          'display'  => $custom_data['prescription_data']['right_axis']
      );
      $other_data[] =   array(
          'name' => 'Left Eye Sphere(SPH)',
          'display'  => $custom_data['prescription_data']['left_eye_sphere']
      );
      $other_data[] =   array(
          'name' => 'Left Eye Cylinder(CYL)',
          'display'  => $custom_data['prescription_data']['left_eye_cylinder']
      );
      $other_data[] =   array(
          'name' => 'Left Eye Axis',
          'display'  => $custom_data['prescription_data']['left_axis']
      );
      if(!empty($custom_data['prescription_data']['single_pd'])){
        $other_data[] =   array(
            'name' => 'Single Eye Pupillary Distance(PD)',
            'display'  => $custom_data['prescription_data']['single_pd']
        );
      } else if(!empty($custom_data['prescription_data']['left_pd']) && !empty($custom_data['prescription_data']['right_pd'])){
        $other_data[] =   array(
            'name' => 'Left Eye Pupillary Distance(PD)',
            'display'  => $custom_data['prescription_data']['left_pd']
        );
        $other_data[] =   array(
            'name' => 'Right Eye Pupillary Distance(PD)',
            'display'  => $custom_data['prescription_data']['right_pd']
        );
      }
      if(!empty($custom_data['prescription_upload'])){
        for($i = 0; $i < sizeof($custom_data['uploaded_files']); $i++){
          $other_data[] =   array(
            'name' => 'File_'.$i,
            'display'  => '<a target="_blank" href="'.$custom_data['uploaded_files'][$i]['url'].'">'.$custom_data['uploaded_files'][$i]['title'].'</a>'
          );
        }
      }
    }
  }
  return $other_data;
}

function get_dropdown($label, $attr_terms){
  ?>
  <ul class="muunel__dropdown">
      <?php
      foreach($attr_terms as $term_id => $term){
        ?>
        <li><input type="radio" name="<?=$label?>" id="<?=$label.'-'.$term_id?>" value="<?=esc_attr($term['value'])?>"><span><svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 6.5L4.5 10.5L13.5 1" stroke="black" stroke-width="1.5"/></svg></span><label for="<?=$label.'-'.$term_id?>"><?=$term['value']?></label></li>
        <?php
      }
      ?>
    </ul>
  <?php
}

function get_lenses_modal(){
  $attr_terms = get_terms('pa_prescriptions');
  $presc_price = null;
  $axis = array();
  for($i = 0; $i <= 180; $i++){
    $axis[] = array(
      'value' => $i,
    );
  }
  ?>
    <div class="lenses_modal">
      <span class="close">
        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M16.5292 1.47059L1.47041 16.5294" stroke="#0B0B0A" stroke-width="1.5"/>
          <path d="M16.5296 16.5294L1.47082 1.47059" stroke="#0B0B0A" stroke-width="1.5"/>
        </svg>
      </span>
      <span class="lens__back">
        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M10 5L3 12L10 19" stroke="black" stroke-width="1.5"/>
          <path d="M3.5 12H23" stroke="black" stroke-width="1.5"/>
        </svg>
      </span>
      <div class="lenses_modal__main opened">
        <h4><?=__('Select Lens Option', 'muunel');?><span class="price"></span></h4>
        <div class="lenses_modal__steps"><div class="circle active"></div><div class="line"></div><div class="circle"></div></div>
        <div class="lenses_modal__main--buttons">
          <?php if(get_field('lenses__repeater', 'options')): ?>
          <?php foreach(get_field('lenses__repeater', 'options') as $lens_button_id => $lens_button):
              if($lens_button['type'] == 'prescription') $presc_price = $lens_button['additional_price'];
            ?>
            <div class="lens_item">
              <div class="lenses_modal__main--buttons__item" id="lens-<?=$lens_button_id?>">
                <input type="radio" value="<?=esc_attr($lens_button['type'])?>" name="lens-button" id="lens-button-<?=$lens_button_id?>">
                <label for="lens-button-<?=$lens_button_id?>">
                  <div class="lenses_icon"><img src="<?=wp_get_attachment_image_src($lens_button['icon']['id'], 'smallThumbnail')[0]?>" alt="<?=$lens_button['icon']['title']?>"></div>
                  <div class="lenses_description">
                    <p class="paragraph-2"><?=$lens_button['title']?></p>
                    <p class="paragraph-5"><?=$lens_button['desc']?></p>
                  </div>
                  <h5 class="lenses_price" data-price="<?=esc_attr($lens_button['additional_price']);?>"><?= $lens_button['additional_price'] > 0 ? wc_price($lens_button['additional_price']) : __("Free", 'muunel'); ?></h5>
                  <?php if($lens_button['additional_info']): ?>
                    <span class="info_icon">
                      <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="10" cy="10" r="9" stroke="#0B0B0A" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M8.8428 13.9432C8.8428 14.5452 9.3398 15.0422 9.9418 15.0422C10.5438 15.0422 11.0408 14.5452 11.0408 13.9432C11.0408 13.3412 10.5438 12.8442 9.9418 12.8442C9.3398 12.8442 8.8428 13.3412 8.8428 13.9432ZM12.8188 7.67824C12.8188 5.96324 11.4958 4.94824 9.8718 4.94824C9.0248 4.94824 8.3038 5.23524 7.7718 5.72524C7.2118 6.24324 6.9668 6.92924 6.9668 7.38424C6.9668 7.67124 7.0368 7.87424 7.1838 8.02824C7.3308 8.18224 7.5408 8.27324 7.7928 8.27324C8.0588 8.27324 8.2688 8.21724 8.4298 8.04924C8.5278 7.93724 8.6328 7.74824 8.6818 7.58724C8.7658 7.31424 8.8638 7.09724 9.0318 6.92224C9.2558 6.68424 9.5358 6.58624 9.8928 6.58624C10.5648 6.58624 10.9988 7.06224 10.9988 7.67824C10.9988 8.06324 10.8168 8.37124 10.5928 8.65124L9.5498 9.96724C9.1578 10.4572 9.0458 10.8142 9.0458 11.2412C9.0458 11.4932 9.1158 11.7102 9.2628 11.8642C9.4308 12.0392 9.6478 12.1442 9.9418 12.1442C10.2358 12.1442 10.4528 12.0532 10.6208 11.8782C10.7538 11.7382 10.8448 11.5562 10.9008 11.3532C10.9778 11.0592 11.0548 10.8632 11.2998 10.5412L12.1398 9.44224C12.5668 8.88224 12.8188 8.45524 12.8188 7.67824Z" fill="#0B0B0A"/>
                      </svg>
                    </span>
                    <div class="info_icon__content"><?=$lens_button['additional_info']?></div>
                  <?php endif; ?>
                </label>
                <ul>
                  <?php if($lens_button['type'] == "reading") : ?>
                    <?php foreach($attr_terms as $add_id => $reading_option): if($reading_option->slug == "non-prescription" || $reading_option->slug == "prescription") continue;?>
                      <li><input type="radio" name="reading_option-<?=$lens_button_id?>" value="<?=$reading_option->slug?>" id="reading_option-<?=$lens_button_id."_".$add_id?>" <?php if($add_id == 0) echo "checked"; ?>><label for="reading_option-<?=$lens_button_id."_".$add_id?>"><?=$reading_option->name?></label></li>
                    <?php endforeach; ?>
                  <?php elseif($lens_button['additional_options'] && $lens_button['type'] != "prescription") : ?>
                    <?php foreach($lens_button['additional_options'] as $add_id => $additional_option): ?>
                      <li><input type="checkbox" additional-price="<?=$additional_option['price']?>" additional-name="<?=esc_attr($additional_option['title'])?>" name="additional_option-<?=$lens_button_id."_".$add_id?>" id="additional_option-<?=$lens_button_id."_".$add_id?>"><label for="additional_option-<?=$lens_button_id."_".$add_id?>"><?=$additional_option['title']?></label><span class="price"><?=wc_price($additional_option['price'])?></span></li>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </ul>
              </div>
            </div>
          <?php endforeach; ?>
          <p class="paragraph-5"><?=__('*All lenses option come with Blue light protection', 'muunel')?></p>
          <div id="lens__continue" class="button__muunel muunel_add_to_cart"><?=__('Continue', 'muunel')?></div>
          <?php endif; ?>
        </div>
      </div>
      <?php $prescription_lenses = get_field('prescription_lenses', 'options'); ?>
      <div class="lenses_modal__prescription">
        <h4><?=__('Your Prescription', 'muunel');?><span class="price"><?= $presc_price != null ? ' ('.wc_price($presc_price).')' : ''; ?></span></h4>
        <div class="lenses_modal__steps"><div class="circle done"></div><div class="line active"></div><div class="circle active"></div></div>
        <div class="lenses_modal__change">
          <div class="btn-change active" data-target="enter"><?=__('Enter Prescription', 'muunel')?></div>
          <div class="btn-change" data-target="upload"><?=__('Upload Prescription', 'muunel')?></div>
        </div>
        <p style="display:none" class="fillAllError paragraph-2"><?=__('Please fill all required fields', 'muunel');?></p>
        <div class="lenses_modal__prescription--table opened">
          <?php
          $additional_lens_info = get_field('additional_lens_info', 'options');
          ?>
          <table>
            <tr>
              <td></td>
              <td><p class="paragraph-2"><?=__('Sphere (SPH)', 'muunel');?></p>
                <?php if($additional_lens_info['sphere_sph']): ?>
                  <span class="info_icon">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <circle cx="10" cy="10" r="9" stroke="#0B0B0A" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                      <path d="M8.8428 13.9432C8.8428 14.5452 9.3398 15.0422 9.9418 15.0422C10.5438 15.0422 11.0408 14.5452 11.0408 13.9432C11.0408 13.3412 10.5438 12.8442 9.9418 12.8442C9.3398 12.8442 8.8428 13.3412 8.8428 13.9432ZM12.8188 7.67824C12.8188 5.96324 11.4958 4.94824 9.8718 4.94824C9.0248 4.94824 8.3038 5.23524 7.7718 5.72524C7.2118 6.24324 6.9668 6.92924 6.9668 7.38424C6.9668 7.67124 7.0368 7.87424 7.1838 8.02824C7.3308 8.18224 7.5408 8.27324 7.7928 8.27324C8.0588 8.27324 8.2688 8.21724 8.4298 8.04924C8.5278 7.93724 8.6328 7.74824 8.6818 7.58724C8.7658 7.31424 8.8638 7.09724 9.0318 6.92224C9.2558 6.68424 9.5358 6.58624 9.8928 6.58624C10.5648 6.58624 10.9988 7.06224 10.9988 7.67824C10.9988 8.06324 10.8168 8.37124 10.5928 8.65124L9.5498 9.96724C9.1578 10.4572 9.0458 10.8142 9.0458 11.2412C9.0458 11.4932 9.1158 11.7102 9.2628 11.8642C9.4308 12.0392 9.6478 12.1442 9.9418 12.1442C10.2358 12.1442 10.4528 12.0532 10.6208 11.8782C10.7538 11.7382 10.8448 11.5562 10.9008 11.3532C10.9778 11.0592 11.0548 10.8632 11.2998 10.5412L12.1398 9.44224C12.5668 8.88224 12.8188 8.45524 12.8188 7.67824Z" fill="#0B0B0A"/>
                    </svg>
                  </span>
                  <div class="info_icon__content"><?=$additional_lens_info['sphere_sph']?></div>
                <?php endif; ?>
              </td>
              <td><p class="paragraph-2"><?=__('Cylinder (CYL)', 'muunel');?></p>
                <?php if($additional_lens_info['cylinder_cyl']): ?>
                  <span class="info_icon">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <circle cx="10" cy="10" r="9" stroke="#0B0B0A" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                      <path d="M8.8428 13.9432C8.8428 14.5452 9.3398 15.0422 9.9418 15.0422C10.5438 15.0422 11.0408 14.5452 11.0408 13.9432C11.0408 13.3412 10.5438 12.8442 9.9418 12.8442C9.3398 12.8442 8.8428 13.3412 8.8428 13.9432ZM12.8188 7.67824C12.8188 5.96324 11.4958 4.94824 9.8718 4.94824C9.0248 4.94824 8.3038 5.23524 7.7718 5.72524C7.2118 6.24324 6.9668 6.92924 6.9668 7.38424C6.9668 7.67124 7.0368 7.87424 7.1838 8.02824C7.3308 8.18224 7.5408 8.27324 7.7928 8.27324C8.0588 8.27324 8.2688 8.21724 8.4298 8.04924C8.5278 7.93724 8.6328 7.74824 8.6818 7.58724C8.7658 7.31424 8.8638 7.09724 9.0318 6.92224C9.2558 6.68424 9.5358 6.58624 9.8928 6.58624C10.5648 6.58624 10.9988 7.06224 10.9988 7.67824C10.9988 8.06324 10.8168 8.37124 10.5928 8.65124L9.5498 9.96724C9.1578 10.4572 9.0458 10.8142 9.0458 11.2412C9.0458 11.4932 9.1158 11.7102 9.2628 11.8642C9.4308 12.0392 9.6478 12.1442 9.9418 12.1442C10.2358 12.1442 10.4528 12.0532 10.6208 11.8782C10.7538 11.7382 10.8448 11.5562 10.9008 11.3532C10.9778 11.0592 11.0548 10.8632 11.2998 10.5412L12.1398 9.44224C12.5668 8.88224 12.8188 8.45524 12.8188 7.67824Z" fill="#0B0B0A"/>
                    </svg>
                  </span>
                  <div class="info_icon__content"><?=$additional_lens_info['cylinder_cyl']?></div>
                <?php endif; ?>
              </td>
              <td><p class="paragraph-2"><?=__('Axis', 'muunel');?></p>
                <?php if($additional_lens_info['axis']): ?>
                  <span class="info_icon">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <circle cx="10" cy="10" r="9" stroke="#0B0B0A" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                      <path d="M8.8428 13.9432C8.8428 14.5452 9.3398 15.0422 9.9418 15.0422C10.5438 15.0422 11.0408 14.5452 11.0408 13.9432C11.0408 13.3412 10.5438 12.8442 9.9418 12.8442C9.3398 12.8442 8.8428 13.3412 8.8428 13.9432ZM12.8188 7.67824C12.8188 5.96324 11.4958 4.94824 9.8718 4.94824C9.0248 4.94824 8.3038 5.23524 7.7718 5.72524C7.2118 6.24324 6.9668 6.92924 6.9668 7.38424C6.9668 7.67124 7.0368 7.87424 7.1838 8.02824C7.3308 8.18224 7.5408 8.27324 7.7928 8.27324C8.0588 8.27324 8.2688 8.21724 8.4298 8.04924C8.5278 7.93724 8.6328 7.74824 8.6818 7.58724C8.7658 7.31424 8.8638 7.09724 9.0318 6.92224C9.2558 6.68424 9.5358 6.58624 9.8928 6.58624C10.5648 6.58624 10.9988 7.06224 10.9988 7.67824C10.9988 8.06324 10.8168 8.37124 10.5928 8.65124L9.5498 9.96724C9.1578 10.4572 9.0458 10.8142 9.0458 11.2412C9.0458 11.4932 9.1158 11.7102 9.2628 11.8642C9.4308 12.0392 9.6478 12.1442 9.9418 12.1442C10.2358 12.1442 10.4528 12.0532 10.6208 11.8782C10.7538 11.7382 10.8448 11.5562 10.9008 11.3532C10.9778 11.0592 11.0548 10.8632 11.2998 10.5412L12.1398 9.44224C12.5668 8.88224 12.8188 8.45524 12.8188 7.67824Z" fill="#0B0B0A"/>
                    </svg>
                  </span>
                  <div class="info_icon__content"><?=$additional_lens_info['axis']?></div>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <td><p class="paragraph-2"><?=__('OD', 'muunel');?></p><p class="paragraph-5"><?=__('Right Eye', 'muunel');?></p></td>
              <td>
                <p id="presc-lens-0" class="muunel__dropdown__trigger right_eye_sphere right"><?=__('Select Sphere', 'muunel');?><span class="arrow"></span></p>
                <?php get_dropdown('right_eye_sphere', $prescription_lenses['right_eye_sphere']);?>
              </td>
              <td>
                <p id="presc-lens-1" class="muunel__dropdown__trigger right_eye_cylinder right"><?=__('Select Cylinder', 'muunel');?><span class="arrow"></span></p>
                <?php get_dropdown('right_eye_cylinder', $prescription_lenses['right_eye_cylinder']);?>
              </td>
              <td class="disabled">
                <p id="presc-lens-2" class="muunel__dropdown__trigger right_axis"><?=__('Right Axis', 'muunel');?><span class="arrow"></span></p>
                <?php get_dropdown('right_axis', $axis);?>
              </td>
            </tr>
            <tr>
              <td><p class="paragraph-2"><?=__('OS', 'muunel');?></p><p class="paragraph-5"><?=__('Left Eye', 'muunel');?></p></td>
              <td>
                <p id="presc-lens-3" class="muunel__dropdown__trigger left_eye_sphere left"><?=__('Select Sphere', 'muunel');?><span class="arrow"></span></p>
                <?php get_dropdown('left_eye_sphere', $prescription_lenses['left_eye_sphere']);?>
              </td>
              <td>
                <p id="presc-lens-4" class="muunel__dropdown__trigger left_eye_cylinder left"><?=__('Select Cylinder', 'muunel');?><span class="arrow"></span></p>
                <?php get_dropdown('left_eye_cylinder', $prescription_lenses['left_eye_cylinder']);?>
              </td>
              <td class="disabled">
                <p id="presc-lens-5" class="muunel__dropdown__trigger left_axis"><?=__('Left Axis', 'muunel');?><span class="arrow"></span></p>
                <?php get_dropdown('left_axis', $axis);?>
              </td>
            </tr>
            <tr>
              <td><p class="paragraph-2"><?=__('PD', 'muunel');?></p><p class="paragraph-5"><?=__('Popillary Distance', 'muunel');?></p></td>
              <td class="single_pd">
                <p class="muunel__dropdown__trigger single_pd"><?=__('Single PD', 'muunel');?><span class="arrow"></span></p>
                <?php get_dropdown('single_pd', $prescription_lenses['single_pd']);?>
              </td>
              <td class="left_pd">
                <p class="muunel__dropdown__trigger left_pd"><?=__('Left PD', 'muunel');?><span class="arrow"></span></p>
                <?php get_dropdown('left_pd', $prescription_lenses['left_pd']);?>
              </td>
              <td class="right_pd">
                <p class="muunel__dropdown__trigger right_pd"><?=__('Right PD', 'muunel');?><span class="arrow"></span></p>
                <?php get_dropdown('right_pd', $prescription_lenses['right_pd']);?>
              </td>
              <td colspan="2" class="last"><input type="checkbox" name="two_pd" id="two_pd">
                <label for="two_pd"><?=__('Two PD numbers', 'muunel');?>
                  <?php if($additional_lens_info['two_pd_numbers']): ?>
                    <span class="info_icon">
                      <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="10" cy="10" r="9" stroke="#0B0B0A" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M8.8428 13.9432C8.8428 14.5452 9.3398 15.0422 9.9418 15.0422C10.5438 15.0422 11.0408 14.5452 11.0408 13.9432C11.0408 13.3412 10.5438 12.8442 9.9418 12.8442C9.3398 12.8442 8.8428 13.3412 8.8428 13.9432ZM12.8188 7.67824C12.8188 5.96324 11.4958 4.94824 9.8718 4.94824C9.0248 4.94824 8.3038 5.23524 7.7718 5.72524C7.2118 6.24324 6.9668 6.92924 6.9668 7.38424C6.9668 7.67124 7.0368 7.87424 7.1838 8.02824C7.3308 8.18224 7.5408 8.27324 7.7928 8.27324C8.0588 8.27324 8.2688 8.21724 8.4298 8.04924C8.5278 7.93724 8.6328 7.74824 8.6818 7.58724C8.7658 7.31424 8.8638 7.09724 9.0318 6.92224C9.2558 6.68424 9.5358 6.58624 9.8928 6.58624C10.5648 6.58624 10.9988 7.06224 10.9988 7.67824C10.9988 8.06324 10.8168 8.37124 10.5928 8.65124L9.5498 9.96724C9.1578 10.4572 9.0458 10.8142 9.0458 11.2412C9.0458 11.4932 9.1158 11.7102 9.2628 11.8642C9.4308 12.0392 9.6478 12.1442 9.9418 12.1442C10.2358 12.1442 10.4528 12.0532 10.6208 11.8782C10.7538 11.7382 10.8448 11.5562 10.9008 11.3532C10.9778 11.0592 11.0548 10.8632 11.2998 10.5412L12.1398 9.44224C12.5668 8.88224 12.8188 8.45524 12.8188 7.67824Z" fill="#0B0B0A"/>
                      </svg>
                    </span>
                    <div class="info_icon__content"><?=$additional_lens_info['two_pd_numbers']?></div>
                  <?php endif; ?>
                </label>
              </td>
            </tr>
          </table>
        </div>
        <div class="lenses_modal__prescription--upload">
          <div class="file-upload" id="product-upload" error-max-files="<?=esc_attr(__('Max file reached', 'muunel'));?>">
            <svg width="35" height="35" viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M10.416 21.75C9.15941 21.75 6.55168 21.75 5.45768 21.75C3.50167 21.75 1.91602 20.0202 1.91602 17.8864C1.91602 15.7525 3.50167 14.0227 5.45768 14.0227C5.70047 14.0227 5.93755 14.0494 6.1666 14.1001C6.16621 14.0744 6.16602 14.0486 6.16602 14.0227C6.16602 11.4621 8.06881 9.38636 10.416 9.38636C11.0088 9.38636 11.5732 9.51875 12.0858 9.7579C12.8576 6.86446 15.3063 4.75 18.2077 4.75C21.0788 4.75 23.5066 6.82053 24.3048 9.6675C24.8399 9.48482 25.4091 9.38636 25.9994 9.38636C29.129 9.38636 31.666 12.1541 31.666 15.5682C31.666 18.9823 29.129 21.75 25.9994 21.75C25.0866 21.75 25.5548 21.75 24.5827 21.75" stroke="#99B09B" stroke-width="1.5"/>
              <path d="M17.5 28.8332L17.5 14.6665" stroke="#99B09B" stroke-width="1.5"/>
              <path d="M13.25 17.5L17.5 13.25L21.75 17.5" stroke="#99B09B" stroke-width="1.5"/>
            </svg>
            <p class="paragraph-3"><?=__('Drag and Drop File Here', 'muunel');?></p>
            <p class="paragraph-5"><?=__('Or', 'muunel');?></p>
            <input type="file" id="fileElem" multiple accept="application/pdf,image/jpeg,image/png">
            <label class="button__muunel" for="fileElem"><?=__('Browse', 'muunel');?></label>
          </div>
          <div id="product-upload-files" class="uploaded-files"></div>
        </div>
        <div id="lens__buy" class="button__muunel muunel_add_to_cart"><?=__('Continue', 'muunel')?></div>
      </div>
    </div>
  <?php
}
add_filter('woocommerce_default_address_fields', 'custom_default_address_fields', 20, 1);
function custom_default_address_fields( $address_fields ){
    if( ! is_cart()){ // <== On cart page only
        // Change class
        $address_fields['first_name']['class'] = array('form-row-first'); //  50%
        $address_fields['last_name']['class']  = array('form-row-last');  //  50%
        $address_fields['address_1']['class']  = array('form-row-wide');  // 100%
        $address_fields['state']['class']      = array('form-row-wide');  // 100%
        $address_fields['postcode']['class']   = array('form-row-first'); //  50%
        $address_fields['city']['class']       = array('form-row-last');  //  50%
    }
    return $address_fields;
}

function get_login_modal(){
  ?>
  <div class="loginModalContainer">
    <span class="close">
      <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.5292 1.47059L1.47041 16.5294" stroke="#0B0B0A" stroke-width="1.5"/>
        <path d="M16.5296 16.5294L1.47082 1.47059" stroke="#0B0B0A" stroke-width="1.5"/>
      </svg>
    </span>
    <h2><?=__('Login', 'muunel');?></h2>
    <div class="socialMediaLogin">
      <?= do_shortcode('[Heateor_Social_Login]'); ?>
      <div class="socialMediaLogin__or">
        <span class="line"></span>
        <span class="text"><?=__('Or login with email', 'muunel');?></span>
        <span class="line"></span>
      </div>
    </div>
    <form class="woocommerce-form woocommerce-form-login" method="post">
      <?php do_action( 'woocommerce_login_form_start' ); ?>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
          <label for="username"><?php esc_html_e( 'Email', 'muunel' ); ?></label>
          <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
          <label for="password"><?php esc_html_e( 'Password', 'muunel' ); ?></label>
          <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password" />
          <a id="lost_password"><?php esc_html_e( 'Forgot Password?', 'muunel' ); ?></a>
        </p>
        <?php do_action( 'woocommerce_login_form' ); ?>
        <p class="form-row">
          <label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
            <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /><span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
          </label>
          <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
          <button type="submit" class="button__muunel" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button>
        </p>
        <p class="sign"><?=__('Don’t Have an Account?', 'muunel');?>&nbsp;<a id="sign_up--trigger"><?=__('Sign Up', 'muunel');?></a></p>
      <?php do_action( 'woocommerce_login_form_end' ); ?>
    </form>
    <?php if(is_checkout()): ?>
      <div class="contunue_as_guest">
        <p><?=__('Continue as a guest', 'muunel');?></p>
      </div>
    <?php endif; ?>
  </div>
  <?php
}

function get_register_modal(){
  ?>
  <div class="loginModalContainer">
    <span class="close">
      <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.5292 1.47059L1.47041 16.5294" stroke="#0B0B0A" stroke-width="1.5"/>
        <path d="M16.5296 16.5294L1.47082 1.47059" stroke="#0B0B0A" stroke-width="1.5"/>
      </svg>
    </span>
    <h2><?=__('Sign Up', 'muunel');?></h2>
    <div class="socialMediaLogin">
      <?= do_shortcode('[Heateor_Social_Login]'); ?>
      <div class="socialMediaLogin__or">
        <span class="line"></span>
        <span class="text"><?=__('Or Sign Up with email', 'muunel');?></span>
        <span class="line"></span>
      </div>
    </div>
    <form method="post" class="woocommerce-form woocommerce-form-register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >
      <?php do_action( 'woocommerce_register_form_start' ); ?>
      <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
        <label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?></label>
        <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
      </p>
      <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
        <p class="woocommerce-form-row form-row woocommerce-form-row--password">
          <label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?></label>
          <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" />
        </p>
        <p class="woocommerce-form-row form-row woocommerce-form-row--password">
          <label for="reg_password2"><?php esc_html_e( 'Confirm password', 'woocommerce' ); ?></label>
          <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password2" id="reg_password2" />
        </p>
      <?php else : ?>
        <p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>
      <?php endif; ?>
      <?php do_action( 'woocommerce_register_form' ); ?>
      <p class="woocommerce-form-row form-row">
        <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
        <button type="submit" class="button__muunel" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
      </p>
      <p class="sign"><?=__('Already Have An account?', 'muunel');?>&nbsp;<a id="sign_in--trigger"><?=__('Sign In', 'muunel');?></a></p>
      <?php do_action( 'woocommerce_register_form_end' ); ?>
    </form>
    <?php if(is_checkout()): ?>
      <div class="contunue_as_guest">
        <p><?=__('Continue as a guest', 'muunel');?></p>
      </div>
    <?php endif; ?>
  </div>
  <?php
}

function get_lostpassword_modal(){
  ?>
  <div class="loginModalContainer">
    <span class="close">
      <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.5292 1.47059L1.47041 16.5294" stroke="#0B0B0A" stroke-width="1.5"/>
        <path d="M16.5296 16.5294L1.47082 1.47059" stroke="#0B0B0A" stroke-width="1.5"/>
      </svg>
    </span>
    <h2><?=__('Recovery Password', 'muunel');?></h2>
    <form method="post" class="woocommerce-ResetPassword lost_reset_password">
      <p class="lost_reset_password--notice">
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="12" cy="12" r="9" stroke="#FBBC05" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
          <path d="M10.7046 6.2H13.3086L12.8746 12.598H11.1386L10.7046 6.2ZM12.0066 16.112C11.6239 16.112 11.3019 15.9907 11.0406 15.748C10.7886 15.496 10.6626 15.1927 10.6626 14.838C10.6626 14.4833 10.7886 14.1893 11.0406 13.956C11.2926 13.7133 11.6146 13.592 12.0066 13.592C12.3986 13.592 12.7206 13.7133 12.9726 13.956C13.2246 14.1893 13.3506 14.4833 13.3506 14.838C13.3506 15.1927 13.2199 15.496 12.9586 15.748C12.7066 15.9907 12.3892 16.112 12.0066 16.112Z" fill="#FBBC05"/>
        </svg>
        <?=__( 'Enter E-mail, which you used for entering the service', 'muunel' ); ?></p><?php // @codingStandardsIgnoreLine ?>
      <p class="woocommerce-form-row form-row">
        <label for="user_login"><?= __( 'Your Email', 'muunel' ); ?></label>
        <input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="user_login" id="user_login" autocomplete="username" />
      </p>
      <div class="clear"></div>
      <?php do_action( 'woocommerce_lostpassword_form' ); ?>
      <p class="woocommerce-form-row form-row">
        <input type="hidden" name="wc_reset_password" value="true" />
        <button type="submit" class="button__muunel" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>"><?php esc_html_e( 'Reset password', 'woocommerce' ); ?></button>
      </p>
      <p class="sign"><a id="back_login--trigger"><?=__('Back to login', 'muunel');?></a></p>
      <?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>
    </form>
  </div>
  <?php
}

function get_prescription_name_by_slug($slug){
  $terms = get_terms("pa_prescriptions");
  foreach($terms as $term){
    if($term->slug == $slug) return $term->name;
  }
  return "";
}

/**
 * Find matching product variation
 *
 * @param $product_id
 * @param $attributes
 * @return int
 */
function find_matching_product_variation_id($product_id, $attributes){
    return (new \WC_Product_Data_Store_CPT())->find_matching_product_variation(
        new \WC_Product($product_id),
        $attributes
    );
}

//get Variant ID by product ID and color attribute
function custom_get_variant_id ($product_id, $color) {
  $id = find_matching_product_variation_id($product_id, [
    'attribute_pa_color' => $color,
    'attribute_pa_prescriptions' => 'non-prescription',
  ]);

  return $id;
}

