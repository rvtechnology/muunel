<?php
/**
 * Flexible Content module - sectionWysiwyg
 */
?>

<?php if($section['is_active']) : ?>
<section class="section sectionWysiwyg">
  <div class="sectionWysiwyg__wrapper">
    <?=$section['wysiwyg']; ?>
  </div>
</section>
<?php endif; ?>
