wp.domReady( () => {
	// Buttons
	wp.blocks.unregisterBlockStyle(
		'core/button',
		[ 'default', 'outline', 'squared', 'fill' ]
	);
	wp.blocks.registerBlockStyle( 'core/button', [
		{
			name: 'muunel',
			label: 'Muunel button',
			isDefault: true,
		}
	]);
	// Images
	wp.blocks.registerBlockStyle( 'core/image', [
		{
			name: 'muunel-cute',
			label: 'Muunel cute effect',
			isDefault: true,
		}
	]);
	wp.blocks.registerBlockStyle( 'core/image', [
		{
			name: 'muunel-full-width',
			label: 'Muunel Full Width',
			isDefault: true,
		}
	]);
	// Headers
	wp.blocks.registerBlockStyle( 'core/heading', [
		{
			name: 'default',
			label: 'Muunel default',
			isDefault: true,
		}
	]);
	wp.blocks.registerBlockStyle( 'core/heading', [
		{
			name: 'muunel',
			label: 'Muunel cute effect'
		}
	]);
	// Table
	wp.blocks.registerBlockStyle( 'core/table', [
		{
			name: 'muunel',
			label: 'Muunel table'
		}
	]);
	// List
	wp.blocks.registerBlockStyle( 'core/list', [
		{
			name: 'muunel',
			label: 'Muunel list'
		}
	]);
	// Columns
	wp.blocks.registerBlockStyle( 'core/columns', [
		{
			name: 'muunel',
			label: 'Muunel columns'
		}
	]);
	// Quotes
	wp.blocks.registerBlockStyle( 'core/quote', [
		{
			name: 'muunel',
			label: 'Muunel quote'
		}
	]);
	// Separator
	wp.blocks.unregisterBlockStyle(
		'core/separator',
		[ 'default', 'dots', 'wide' ]
	);
	wp.blocks.registerBlockStyle( 'core/separator', [
		{
			name: 'muunel',
			label: 'Muunel Separator',
			isDefault: true,
		}
	]);
});