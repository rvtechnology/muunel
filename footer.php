<?php
	$footer_fields = get_field('footer_setting', 'options');
	$lang_code = apply_filters( 'wpml_current_language', NULL );
?>
	<footer class="footer no-print">
		<div class="alignfull footer__columns">
			<div class="footer__column">
				<div class="footer__column--title" data-target="column_1"><?=$footer_fields['column_1_title'] ? $footer_fields['column_1_title'] : '';?><span class="btn_footer"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 4V20" stroke="white" stroke-width="2"/><path d="M20 12H4" stroke="white" stroke-width="2"/></svg></span></div>
				<div class="footer__column--links" data-target="column_1">
					<?php if($footer_fields['column_1_repeater']):
					foreach($footer_fields['column_1_repeater'] as $link) { ?>
						<div class="footer__column--links__item"><a href="<?=$link['url']['url']?>"><?=$link['url']['title']?></a></div>
						<?php
					} endif; ?>
				</div>
			</div>
			<div class="footer__column">
				<div class="footer__column--title" data-target="column_2"><?=$footer_fields['column_2_title'] ? $footer_fields['column_2_title'] : '';?><span class="btn_footer"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 4V20" stroke="white" stroke-width="2"/><path d="M20 12H4" stroke="white" stroke-width="2"/></svg></span></div>
				<div class="footer__column--links" data-target="column_2">
					<?php if($footer_fields['column_2_repeater']):
					foreach($footer_fields['column_2_repeater'] as $link) { ?>
						<div class="footer__column--links__item"><a href="<?=$link['url']['url']?>"><?=$link['url']['title']?></a></div>
						<?php
					} endif; ?>
				</div>
			</div>
			<div class="footer__column">
				<div class="footer__column--title" data-target="column_3"><?=$footer_fields['column_3_title'] ? $footer_fields['column_3_title'] : '';?><span class="btn_footer"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 4V20" stroke="white" stroke-width="2"/><path d="M20 12H4" stroke="white" stroke-width="2"/></svg></span></div>
				<div class="footer__column--links" data-target="column_3">
					<?php if($footer_fields['column_3_repeater']):
					foreach($footer_fields['column_3_repeater'] as $link) { ?>
						<div class="footer__column--links__item"><a href="<?=$link['url']['url']?>"><?=$link['url']['title']?></a></div>
						<?php
					} endif; ?>
				</div>
			</div>
			<div class="footer__column">
				<div class="footer__column--title" data-target="column_4"><?=$footer_fields['column_4_title'] ? $footer_fields['column_4_title'] : '';?><span class="btn_footer"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 4V20" stroke="white" stroke-width="2"/><path d="M20 12H4" stroke="white" stroke-width="2"/></svg></span></div>
				<div class="footer__column--links" data-target="column_4">
					<?php if($footer_fields['column_4_repeater']):
					foreach($footer_fields['column_4_repeater'] as $link) { ?>
						<div class="footer__column--links__item"><a href="<?=$link['url']['url']?>"><img src="<?=$link['icon']['url']?>" alt="<?=$link['icon']['title']?>"><?=$link['url']['title']?></a></div>
						<?php
					} endif; ?>
					<div class="footer__newsletter">
					<div class="footer__column--title mt-30"><?=__('Subscribe Us Now', 'muunel')?></div>
						<?=get_sidebar('footer');?>
					</div>
				</div>
			</div>
			<div class="footer__copywrite">
				<div class="footer__copywrite--label"><?=$footer_fields['copywrite_label'] ? $footer_fields['copywrite_label'] : '';?></div>
				<?php if($footer_fields['copywrite_repeater']):
				foreach($footer_fields['copywrite_repeater'] as $link) { ?>
					<div class="footer__copywrite--item"><a href="<?=$link['url']['url']?>" target="<?=$link['url']['target'];?>"><?=$link['url']['title']?></a></div>
					<?php
				} endif; ?>
				<div class="footer__copywrite--language"><?php echo __('Language:', 'muunel'); do_action( 'wpml_add_language_selector' ); ?></div>
			</div>
		</div>
	</footer>
	<?php
	if(is_shop() || is_product_category()) { ?>
		<div id="loading" class="customArchiveProduct__loading modal-container">
			<div class="circle">
				<svg class="circle__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
					<circle class="circle__svg-circle" cx="50" cy="50" r="45"/>
				</svg>
			</div>
		</div>
		<div class="modal-container" id="filters_mobile_modal"></div>
	<?php }
	if(is_product()){
		?>
		<div class="modal-container" id="reviewModal">
			<div class="modal-content">
				<span class="close closeReview">
					<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M16.5292 1.47059L1.47041 16.5294" stroke="#0B0B0A" stroke-width="1.5"/>
						<path d="M16.5296 16.5294L1.47082 1.47059" stroke="#0B0B0A" stroke-width="1.5"/>
					</svg>
				</span>
				<h5><?php echo __("Write a Review", "muunel");?></h5>
				<?php global $wp_query; ?>
				<?=do_shortcode('[site_reviews_form assigned_posts="'.$wp_query->post->ID.'" class="review_form" ]'); ?>
			</div>
		</div>

        <div class="modal-container measurementsModal" id="measurementsModal" >
            <?php global $wp_query; ?>
            <div class="modal-content">
				<span class="close closeReview">
					<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M16.5292 1.47059L1.47041 16.5294" stroke="#0B0B0A" stroke-width="1.5"/>
						<path d="M16.5296 16.5294L1.47082 1.47059" stroke="#0B0B0A" stroke-width="1.5"/>
					</svg>
				</span>
                <div class="measurementsModal__dynamicInfo">
                    <h2><?php echo __("Frame Measurements", "muunel");?></h2>
                    <h3><?=get_the_title($wp_query->post->ID); ?></h3>
                    <p class="measurementsModal__textFrameSize"><b><?php echo __("Frame Size", "muunel");?>:</b> <span></span></p>
                    <p class="measurementsModal__textStandardMeasurements"><b><?php echo __("Standard Measurements", "muunel");?>:</b> <span></span></p>
                    <p class="measurementsModal__textTotalFrameWidth"><b><?php echo __("Total Frame Width", "muunel");?>:</b> <span></span></p>
                    <img src="" alt="" class="measurementsModal__dynamicImg">
                </div>

                <?php $frame_measurements = get_field('frame_measurements', $wp_query->post->ID); ?>
                <?php foreach($frame_measurements as $element) : ?>
                    <div class="measurementsModal__staticInfoBlock">
                        <img src="<?= $element['image']['sizes']['productCartThumbnail']; ?>" alt="">
                        <div class="wysiwyg">
                            <?= $element['text']; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
		<?php
	}
	?>
	<?php 
	if( get_post_meta( get_the_ID(), 'sharing_disabled', true ) == ""){
		?>
		<div class="modal-container" id="shareModal">
			<div class="modal-content">
				<span class="close closeShare">
					<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M16.5292 1.47059L1.47041 16.5294" stroke="#0B0B0A" stroke-width="1.5"/>
						<path d="M16.5296 16.5294L1.47082 1.47059" stroke="#0B0B0A" stroke-width="1.5"/>
					</svg>
				</span>
				<h5><?=__("Share This", "muunel");?></h5>
				<div class="product-info">
				<?php 
				if(is_product()): ?>
					<?php global $product; ?>
					<img src="" alt="<?=$product->get_title();?>"/>
					<h3><?=$product->get_title();?></h3>
				<?php 
				endif;
				?>
				<?php 
				if(!is_product()) : ?>
					<img src="<?php echo get_the_post_thumbnail_url( get_the_ID() );?>" alt="">
					<h3><?php echo get_the_title( get_the_ID() );?></h3>
				<?php 
				endif;
				?>
				</div>
				<?=do_shortcode('[addtoany]'); ?>
				<div class="separator">
					<span class="grey-line"></span>
					<p><?=__("OR", "muunel"); ?></p>
					<span class="grey-line"></span>
				</div>
				<div class="copyBox">
					<p id="copy-text"><?=get_permalink(); ?></p>
					<div id="copy-to-clipboard">
						<svg width="28" height="29" viewBox="0 0 28 29" fill="none" xmlns="http://www.w3.org/2000/svg">
							<g filter="url(#filter0_d)">
							<path d="M16 5V3H5V18H10" stroke="#0B0B0A" stroke-width="1.5"/>
							<rect x="12" y="7" width="11" height="15" stroke="#0B0B0A" stroke-width="1.5"/>
							</g>
							<defs>
							<filter id="filter0_d" x="-2" y="-2" width="32" height="32" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
							<feFlood flood-opacity="0" result="BackgroundImageFix"/>
							<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
							<feOffset dy="2"/>
							<feGaussianBlur stdDeviation="2"/>
							<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.102109 0"/>
							<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
							<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
							</filter>
							</defs>
						</svg>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	?>
	<?php if(!is_cart()){
		?><div id="mini-cart" class="modal-container"><?=get_mini_cart()?></div><?php
	} ?>
	<?php if(is_product()){ ?>
		<div id="lenses-modal" class="modal-container"><?=get_lenses_modal()?></div>
		<div id="lightbox-gallery-modal" class="modal-container">
			<div class="lightboxGalleryContainer">
				<span class="close">
					<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M16.5292 1.47059L1.47041 16.5294" stroke="#0B0B0A" stroke-width="1.5"/>
						<path d="M16.5296 16.5294L1.47082 1.47059" stroke="#0B0B0A" stroke-width="1.5"/>
					</svg>
				</span>
				<div id="lightboxGalleryNavigation" class="lightboxGalleryContainer__nav splide">
					<div class="splide__track">
						<ul class="splide__list"></ul>
					</div>
				</div>
				<div id="lightboxGalleryContent" class="lightboxGalleryContainer__content splide">
					<div class="splide__track">
						<ul class="splide__list"></ul>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	<?php if(!is_user_logged_in()){
		?>
		<div id="login-modal" class="modal-container"><?=get_login_modal();?></div>
		<div id="register-modal" class="modal-container"><?=get_register_modal();?></div>
		<div id="lostpassword-modal" class="modal-container"><?=get_lostpassword_modal();?></div>
		<?php
	} ?>
	<script>
		window.fwSettings={
			'widget_id':67000002994
		};
		!function(){if("function"!=typeof window.FreshworksWidget){var n=function(){n.q.push(arguments)};n.q=[],window.FreshworksWidget=n}}() 
		
	
	</script>
	<script type='text/javascript' src='https://widget.freshworks.com/widgets/67000002994.js' async defer></script>
	<?php wp_footer(); ?>
	
	
	<script>

	    jQuery('li.color-variable-item').click(function(e) 
   { 
       	  setTimeout(function(){
    jQuery( "#primary-slider-list li" ).addClass( "primaryclass" );
       	  }, 5000);
   });
   
   

     // jQuery(".custom_splide li").on( "click", function() {
          
          jQuery(document.body).on('click', '.custom_splide li' ,function(){

          
          jQuery(".custom_splide li").removeClass("active");
           jQuery(this).addClass("active");  
 
    var theID = jQuery(this).attr("data-id"),
        $allItem = jQuery('.primaryclass')
        var $currItem = jQuery('.primaryclass[data-id=' + theID + ']');
    jQuery('.primaryclass[data-id=' + theID + ']').addClass('is-active is-visible ');
    $allItem.not($currItem).removeClass('is-active is-visible');
});
jQuery(function(){
    jQuery('.customer-ques').attr('id', 'customer-ques-id');
    jQuery('#button').click(function(){ 
       
                jQuery('#iframeHolder').html('<iframe id="iframe" src="https://widget.trustpilot.com/trustboxes/5418052cfbfb950d88702476/popup.html" width="400" height="450"></iframe>');
        
    });   
});
</script>

	
	
</body>
</html>
