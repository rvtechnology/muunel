<?php
$minicart_acf = get_field('mini_cart', 'options');
$payments_icons = get_field('payments_icon', 'options');
// WC()->cart->calculate_shipping();

$shipping_cost = WC()->cart->get_shipping_total();
if($shipping_cost == 0) $shipping_cost = __('Free', 'muunel');
else $shipping_cost = wc_price($shipping_cost);

$subtotal = WC()->cart->get_cart_subtotal();
$discount_total = WC()->cart->get_discount_total();
$is_coupon = sizeof(WC()->cart->get_applied_coupons());
// WC()->cart->calculate_totals();
$total = WC()->cart->get_total();
?>
<div class="cart-dropdown cart-sidebar">
    <div class="cart-dropdown__wrapper">
        <div class="cart-preview">
            <h3><?=__('Order Summary', 'muunel');?></h3>
            <?php if(is_checkout()): ?>
            <div class="cart-preview__products">
                <?php foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) : ?>
                <?php
                    $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                    $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
                    $parent = wc_get_product($_product->get_parent_id());
                ?>
                <?php if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) : ?>
                    <?php $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key); ?>
                    <div class="cart-preview__products--single">
                        <div class="product-thumbnail">
                            <?php
                            $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);
                            if (!$product_permalink) :
                                echo wp_kses_post($thumbnail);
                            else :
                                printf('<a href="%s">%s</a>', esc_url($product_permalink), wp_kses_post($thumbnail));
                            endif;
                            ?>
                        </div>
                        <div class="product-name">
                            <div class="product-name--title"><?=$parent->get_title();?></div>
                            <div class="product-name--color"><?=$_product->get_attribute('pa_color');?></div>
                        </div>
                        <div class="product-quantity"><?=$cart_item['quantity'];?>x</div>
                        <div class="product-subtotal"><?=wc_price($cart_item['data']->get_price());?></div>
                    </div>
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
        </div>
        <?php if(is_cart()) : do_action( 'woocommerce_before_cart_totals' ); ?>
	    <?php else : do_action( 'woocommerce_checkout_before_order_review' ); endif; ?>
        <div class="cart__summary cart_totals">
            <?php if($shipping_cost == 'Free'):?>
            <p class="free_shipping_label"><?=__('You are elligible for free shipping!', 'muunel')?></p>
            <?php endif; ?>
            <div class="cart__summary--item">
                <p><?=__('Subtotal', 'muunel');?>:</p>
                <p class="mini-cart-value"><?=$subtotal;?></p>
            </div>
            <div class="cart__summary--item">
                <p><?=__('Shipping', 'muunel');?>:</p>
                <p class="mini-cart-value"><?=$shipping_cost?></p>
            </div>
            <?php if($is_coupon != 0) { ?>
                <div class="cart__summary--item">
                <p><?=__('Promocode', 'muunel');?>:</p>
                <p class="mini-cart-value">-<?=wc_price($discount_total)?></p>
                </div>
            <?php } ?>


            <div class="cart__summary--price order-total">
                <p><?=__('Total', 'muunel');?>:</p>
                <p class="mini-cart-value-total"><?=$total;?></p>
            </div>


            <?php if(is_cart()): ?>
            <form class="cart__summary--coupons no-print">
                <?php
                $coupon_placeholder = __('Coupon Code', 'muunel');
                if($is_coupon != 0){
                $coupon_placeholder = "";
                }
                ?>
                <input type="text" id="coupon-input" data-coupon-error="<?=esc_attr(__('Enter valid coupon!', 'muunel'))?>" placeholder="<?=$coupon_placeholder?>" <?php if($is_coupon != 0){ echo 'disabled'; } ?> value="">
                <?php if($is_coupon != 0): ?>
                <p><?=WC()->cart->applied_coupons[0];?>
                    <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 6.5L4.5 10.5L13.5 1" stroke="black" stroke-width="1.5"/>
                    </svg>
                </p>
                <button id="remove_coupon"><?=__('Remove', 'muunel')?></button>
                <?php else: ?>
                <button id="apply_coupon"><?=__('Apply', 'muunel')?></button>
                <?php endif; ?>
            </form>
            <?php endif; ?>
            <?php if(is_cart()) : do_action( 'woocommerce_after_cart_totals' );
            else : do_action( 'woocommerce_checkout_after_order_review' ); endif; ?>


            <div class="cart__summary--desc no-print">
                <ul>
                    <?php if($minicart_acf['repeater']): foreach($minicart_acf['repeater'] as $item){
                    ?>
                        <li class="cart-preview__desc--item paragraph-5">
                        <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 6.5L4.5 10.5L13.5 1" stroke="black" stroke-width="1.5"/>
                        </svg>
                        <?=$item['desc']?>
                        </li>
                    <?php
                    } endif; ?>
                </ul>
            </div>
            <?php if(is_cart()): ?>
            <a class="button__muunel no-print" href="<?php echo wc_get_checkout_url(); ?>">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="2" y="4" width="20" height="16" stroke="white" stroke-width="1.5"/>
                <line x1="2.75" y1="8.25" x2="21.25" y2="8.25" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <line x1="13" y1="15.25" x2="19" y2="15.25" stroke="white" stroke-width="1.5"/>
                <circle cx="6.5" cy="15.5" r="1.5" fill="white"/>
                <circle cx="8.5" cy="15.5" r="1.5" fill="white"/>
                </svg>
                <?=__('Procced To Checkout', 'muunel');?>
            </a>
            <?php endif; ?>
	        <?php if(is_cart() || is_checkout()): ?>
                <?php if($payments_icons): ?>
                    <ul class="avaliable_payments">
                    <?php foreach($payments_icons as $icon): ?>
                        <li><img src="<?=$icon['url']?>" alt="<?=$icon['title']?>"></li>
                    <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                <div class="helpdesk" id="helpdesk-sidebar">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path id="Stroke 1" fill-rule="evenodd" clip-rule="evenodd" d="M11.5317 12.4724C15.5208 16.4604 16.4258 11.8467 18.9656 14.3848C21.4143 16.8328 22.8216 17.3232 19.7192 20.4247C19.3306 20.737 16.8616 24.4943 8.1846 15.8197C-0.493478 7.144 3.26158 4.67244 3.57397 4.28395C6.68387 1.17385 7.16586 2.58938 9.61449 5.03733C12.1544 7.5765 7.54266 8.48441 11.5317 12.4724Z" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    <div class="helpdesk__info">
                        <p class="paragraph-4"><?=__('Need help?', 'muunel');?></p>
                        <p class="paragraph-1"><?=__('Start a live chat!', 'muunel');?></p>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?= apply_filters( 'woocommerce_widget_cart_after_price', null) ?>
    </div>
</div>